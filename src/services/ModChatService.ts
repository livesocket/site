import { injectable } from 'inversify';
import { Socket } from './Socket';
import { channel } from '../helpers/channel';
import { ModChatMessage } from '../models/ModChatMessage';

export type ModChatMessageHandler = (message: ModChatMessage) => any;

@injectable()
export class ModChatService {
  private onMessages: ModChatMessageHandler[] = [];
  private socket = Neon.get<Socket>('Socket');

  constructor() {
    this.socket.subscribe(`public.event.modchat.message.${channel().replace('#', '')}`, ([message]: [ModChatMessage]) =>
      this.onMessages.forEach(x => x(message))
    );
  }
  public onMessage(handler: ModChatMessageHandler) {
    this.onMessages.push(handler);
  }

  public async history(offset: number = 0, limit: number = 50): Promise<ModChatMessage[]> {
    const result = await this.socket.call('public.modchat.get', [], { channel: channel(), offset, limit });
    // tslint:disable-next-line
    console.log('result', result);
    return result.args || [];
  }

  public async send(message: string): Promise<ModChatMessage> {
    return this.socket.call('public.modchat.send', [], { channel: channel(), message });
  }
}
