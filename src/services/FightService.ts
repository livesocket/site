import { injectable, inject } from 'inversify';
import { Socket } from './Socket';
import { channel } from '../helpers/channel';

export interface FightConfig {
  channel: string;
  enabled: boolean;
  cooldown: number;
}

@injectable()
export class FightService {
  private socket = Neon.get<Socket>('Socket');

  public async get(): Promise<FightConfig> {
    return this.socket.call('public.fight.config.getByChannel', [channel()]);
  }

  public async update(config: FightConfig): Promise<FightConfig> {
    return this.socket.call('public.fight.config.update', [config]);
  }
}
