import { injectable, inject } from 'inversify';
import { Socket } from './Socket';
import { channel } from '../helpers/channel';

export interface RollConfig {
  channel: string;
  enabled: boolean;
  cooldown: number;
}

@injectable()
export class RollService {
  private socket = Neon.get<Socket>('Socket');

  public async get(): Promise<RollConfig> {
    return this.socket.call('public.roll.config.getByChannel', [channel()]);
  }

  public async update(config: RollConfig): Promise<RollConfig> {
    return this.socket.call('public.roll.config.update', [config]);
  }
}
