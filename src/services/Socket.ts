import { Connection, IConnectionOptions, Session, ISubscription, SubscribeHandler, ISubscribeOptions } from 'autobahn';
import { injectable } from 'inversify';

export interface ConnectionDetails {
  authid: string;
  authrole: string;
  authextra: any;
}

@injectable()
export class Socket {
  private connection: Connection;
  private session: Session;
  private details: any;

  constructor() {
    // tslint:disable-next-line
    console.log('Creating instance of Socket');
  }

  public connect(token: string): Promise<ConnectionDetails> {
    return this._connect({
      // url: `ws://${location.hostname}:8080`,
      url: `wss://${location.hostname}/ws/`,
      realm: 'chatbot',
      authmethods: ['ticket'],
      authid: 'access_token',
      onchallenge: () => token
    });
  }

  public async call<T = any>(procedure: string, args?: any[], kwargs?: any, options?: any): Promise<T> {
    return await this.session.call<T>(procedure, args, kwargs, options);
  }

  public async subscribe<T = any>(
    topic: string,
    handler: SubscribeHandler,
    options?: ISubscribeOptions
  ): Promise<ISubscription> {
    return await this.session.subscribe(topic, handler, options);
  }

  private _connect(options: IConnectionOptions): Promise<ConnectionDetails> {
    return new Promise<ConnectionDetails>((resolve, reject) => {
      this.connection = new Connection(options);

      this.connection.onopen = (session: Session, details: any) => {
        this.session = session;
        this.details = details;
        resolve(this.details);
      };

      this.connection.onclose = (reason: string, details: any) => {
        this.session = void 0;
        this.details = void 0;
        reject(new Error(reason));
        return true;
      };

      this.connection.open();
    });
  }
}
