import { DateTime } from 'luxon';
import { Pi, PiRaw } from '../models/Pi';
import { injectable } from 'inversify';
import { Socket } from './Socket';

@injectable()
export class PiService {
  private socket = Neon.get<Socket>('Socket');

  public async get(): Promise<Pi[]> {
    const result = await this.socket.call('admin.pi.get');
    return (result.args as PiRaw[]).map(x => ({ ...x, timestamp: DateTime.fromISO(x.timestamp) }));
  }

  public async update(id: string, version: string): Promise<void> {
    return this.socket.call(`pi.${id}.update`, [version]);
  }
}
