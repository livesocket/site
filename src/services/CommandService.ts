import { Command } from '../models/Command';
import { inject, injectable } from 'inversify';
import { Socket } from './Socket';
import { channel } from '../helpers/channel';

@injectable()
export class CommandService {
  private socket = Neon.get<Socket>('Socket');

  public async get(): Promise<Command[]> {
    return this.socket.call('public.command.get', [channel()]);
  }

  public async getById(name: string): Promise<Command> {
    return this.socket.call('public.command.getById', [channel(), name]);
  }

  public async create(command: Command): Promise<Command> {
    return this.socket.call('public.command.create', [{ ...command, channel: channel() }]);
  }

  public async update(command: Command): Promise<Command> {
    return this.socket.call('public.command.update', [command]);
  }

  public async delete(name: string): Promise<void> {
    return this.socket.call('public.command.delete', [channel(), name]);
  }

  public async toggleEnabled(command: Command): Promise<Command> {
    if (command.enabled) {
      return this.socket.call('public.command.disable', [channel(), command.name]);
    } else {
      return this.socket.call('public.command.enable', [channel(), command.name]);
    }
  }

  public async toggleRestricted(command: Command): Promise<Command> {
    if (command.restricted) {
      return this.socket.call('public.command.unrestrict', [channel(), command.name]);
    } else {
      return this.socket.call('public.command.restrict', [channel(), command.name]);
    }
  }

  public async toggleScheduled(command: Command): Promise<Command> {
    if (command.scheduled) {
      return this.socket.call('public.command.unschedule', [channel(), command.name]);
    } else {
      return this.socket.call('public.command.schedule', [channel(), command.name]);
    }
  }
}
