import { injectable, inject } from 'inversify';
import { client_id, redirect_uri } from '../config';
import { Socket } from './Socket';
import { channels, channel } from '../helpers/channel';
import { Toolbar } from '../elements/bricks/toolbar';
import { admin } from '../helpers/admin';

const state = 'kjhsdf76234GFUH3947734sf';

@injectable()
export class LoginService {
  public get token(): string {
    return localStorage.getItem('access_token');
  }

  public set token(token: string) {
    if (!token) {
      localStorage.removeItem('access_token');
    }
    localStorage.setItem('access_token', token);
  }

  public get state(): string {
    return localStorage.getItem('login_state');
  }

  public set state(value: string) {
    if (!value) {
      localStorage.removeItem('login_state');
    }
    localStorage.setItem('login_state', value);
  }

  private socket = Neon.get<Socket>('Socket');

  public async validate(): Promise<boolean> {
    return fetch('https://id.twitch.tv/oauth2/validate', { headers: { Authorization: `OAuth ${this.token}` } }).then(
      res => {
        if (!res.ok) {
          throw new Error('Invalid access token');
        }
        return res.json().then(x => x.login);
      }
    );
  }

  public authorize() {
    location.href = `https://id.twitch.tv/oauth2/authorize?client_id=${client_id}&redirect_uri=${encodeURIComponent(
      redirect_uri
    )}&response_type=token&scope=&state=${state}`;
    this.state = state;
  }

  public async login(token: string = this.token, state: string = this.state) {
    if (state !== this.state) {
      throw new Error('State Mismatch');
    }

    this.token = token;
    const { authrole, authextra } = await this.socket.connect(this.token);
    channels(authextra.channels);
    if (authrole === 'admin') {
      admin(true);
      return Neon.navigate('choose-channel');
    }
    if (channels().length > 1) {
      return Neon.navigate('choose-channel');
    }
    channel(channels()[0]);
    Neon.navigate('home-');
  }
}
