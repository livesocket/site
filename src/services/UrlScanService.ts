import { UrlScan } from '../models/UrlScan';
import { channel } from '../helpers/channel';
import { Socket } from './Socket';
import { injectable } from 'inversify';

@injectable()
export class UrlScanService {
  private socket = Neon.get<Socket>('Socket');

  public async get(): Promise<UrlScan> {
    return this.socket.call('public.urlscan.getByChannel', [channel()]);
  }

  public async update(config: UrlScan): Promise<UrlScan> {
    return this.socket.call('public.urlscan.update', [config]);
  }
}
