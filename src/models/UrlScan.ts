import { PermissionLevel } from './PermissionLevel';

export interface UrlScan {
  channel: string;
  enabled: boolean;
  permit: PermissionLevel;
  useWhitelist: boolean;
  whitelist: string;
  timeout: number;
  createdAt: Date;
  updatedBy: string;
  updatedAt: Date;
}
