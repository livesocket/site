export class ModChatMessage {
  public id: number;
  public name: string;
  public timestamp?: Date;

  constructor(public channel: string, public message: string) {}
}
