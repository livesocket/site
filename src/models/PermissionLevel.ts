export enum PermissionLevel {
  ALL = 0,
  SUB = 1,
  VIP = 2,
  MOD = 3,
  BROADCASTER = 4
}

export const PermissionLevelMap = new Map<number, string>([
  [0, 'All'],
  [1, 'Subscriber'],
  [2, 'VIP'],
  [3, 'Moderator'],
  [4, 'Broadcaster']
]);
