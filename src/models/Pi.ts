import { DateTime } from 'luxon';

export interface PiBase {
  id: string;
  channel: string;
  project: string;
}

export interface Pi extends PiBase {
  timestamp: DateTime;
}
export interface PiRaw extends PiBase {
  timestamp: string;
}
