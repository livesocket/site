import { html, render } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { Element, NeonElement } from '../../lib';

const template = () => html`
  <h1>Admin Panel</h1>
  <h2>Careful these options are dangerous</h2>
`;

@Element('admin-welcome')
export class AdminWelcome extends NeonElement {
  public connectedCallback() {
    render(template(), this);
  }
}
