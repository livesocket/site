import { render, html } from 'lit-html';
import { Element, PopupElement, onEvent } from '../../lib';
import { Pi } from '../../models/Pi';
import { PiService } from '../../services/PiService';

const template = (e: AdminPiUpdate) => html`
  <form @submit=${(event: Event) => e.submit(event)}>
    <form-display label="Channel" value=${e.pi.channel}></form-display>
    <form-display label="ID" value=${e.pi.id}></form-display>
    <form-display label="Project" value=${e.pi.project}></form-display>
    <text-input placeholder="New Version" @change=${onEvent(x => (e.newVersion = x))}></text-input>
    <button type="submit">Send Update</button>
  </form>
`;

@Element('admin-pi-update')
export class AdminPiUpdate extends PopupElement {
  public pi: Pi;
  public newVersion: string;

  public connectedCallback() {
    render(template(this), this);
  }

  public async submit(event: Event) {
    event.preventDefault();
    try {
      await Neon.get<PiService>('PiService').update(this.pi.id, this.newVersion);
      this.complete();
    } catch (error) {
      Neon.handleError(error);
    }
  }
}
