import { html, render } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { Element, NeonElement } from '../../lib';

const template = (e: AdminHome) => html`
  <admin-sidebar>
    <ul>
      <li @click=${() => e.open('admin-channels')}>Channels</li>
      <li @click=${() => e.open('admin-commands')}>Commands</li>
      <li @click=${() => e.open('admin-pi')}>Pis</li>
    </ul>
  </admin-sidebar>
  <admin-container>
    ${unsafeHTML(e.openPage)}
  </admin-container>
`;

@Element('admin-home')
export class AdminHome extends NeonElement {
  public openPage = '<admin-welcome></admin-welcome>';
  public connectedCallback() {
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }

  public open(el: string) {
    this.openPage = `<${el}></${el}>`;
    this.draw();
  }
}
