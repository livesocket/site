import { html, render } from 'lit-html';
import { Element, NeonElement } from '../../lib';
import { PiService } from '../../services/PiService';
import { Pi } from '../../models/Pi';
import { AdminPiUpdate } from './pi-update';

const template = (e: AdminPi) => html`
  <table>
    <thead>
      <tr>
        <th>Channel</th>
        <th>ID</th>
        <th>Project</th>
        <th>Last Ping</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      ${e.pis.map(
        x => html`
          <tr>
            <td>${x.channel}</td>
            <td>${x.id}</td>
            <td>${x.project}</td>
            <td>${x.timestamp.toFormat('f')}</td>
            <td>
              <button type="button" @click=${() => e.update(x)}>Update</button>
            </td>
          </tr>
        `
      )}
    </tbody>
  </table>
`;

@Element('admin-pi')
export class AdminPi extends NeonElement {
  public pis: Pi[];

  public async connectedCallback() {
    this.pis = await Neon.get<PiService>('PiService').get();
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }

  public update(pi: Pi) {
    Neon.popup.show<AdminPiUpdate>('admin-pi-update', { pi });
  }
}
