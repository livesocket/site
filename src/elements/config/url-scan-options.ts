import { render, html } from 'lit-html';
import { Element, NeonElement, onEvent } from '../../lib';
import { UrlScan } from '../../models/UrlScan';
import { UrlScanService } from '../../services/UrlScanService';
import { PermissionLevelMap } from '../../models/PermissionLevel';

const template = (e: UrlScanOptions) => html`
  <category-box title="Url Scan Options">
    <section>
      <label>Scanning Enabled</label>
      <check-box .value=${e.config.enabled} @change=${onEvent(value => e.save((e.config.enabled = value)))}></check-box>
    </section>
    <section>
      <label>Minimum Level</label>
      <select-input
        .value=${e.config.permit}
        .options=${PermissionLevelMap}
        @change=${onEvent(value => e.save((e.config.permit = value)))}
        placeholder="Choose minimum level"
      ></select-input>
    </section>
    <section>
      <label>Use Whitelist</label>
      <check-box
        .value=${e.config.useWhitelist}
        @change=${onEvent(value => e.save((e.config.useWhitelist = value)))}
      ></check-box>
    </section>
    <section>
      <label>Whitelist</label>
      <text-input
        .value=${e.config.whitelist}
        @change=${onEvent(value => e.save((e.config.whitelist = value)))}
      ></text-input>
    </section>
    <section>
      <label>Timeout:</label>
      <number-edit
        .value=${e.config.timeout}
        @commit=${onEvent(timeout => e.save((e.config.timeout = timeout || 0)))}
        hint="None"
        min="0"
      ></number-edit>
    </section>
  </category-box>
`;

@Element('url-scan-options')
export class UrlScanOptions extends NeonElement {
  public config: UrlScan;
  public service = Neon.get<UrlScanService>('UrlScanService');

  public async connectedCallback() {
    try {
      this.config = await this.service.get();
      this.draw();
    } catch (e) {
      Neon.handleError(e);
    }
  }

  public draw() {
    render(template(this), this);
  }

  public async save(_?: any) {
    await this.service.update(this.config);
    this.draw();
  }
}
