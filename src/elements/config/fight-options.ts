import { render, html } from 'lit-html';
import { Element, NeonElement, onEvent } from '../../lib';
import { FightConfig, FightService } from '../../services/FightService';

const template = (e: FightOptions) => html`
  <category-box title="Fight Options">
    <section>
      <label>Fights Enabled</label>
      <check-box .value=${e.config.enabled} @change=${onEvent(value => e.save((e.config.enabled = value)))}></check-box>
    </section>
    <section>
      <label>Fight Cooldown:</label>
      <number-edit
        .value=${e.config.cooldown}
        @commit=${onEvent(cooldown => e.save((e.config.cooldown = cooldown || 0)))}
        hint="None"
        min="0"
      ></number-edit>
    </section>
  </category-box>
`;

@Element('fight-options')
export class FightOptions extends NeonElement {
  public config: FightConfig;
  public service = Neon.get<FightService>('FightService');

  public async connectedCallback() {
    try {
      this.config = await this.service.get();
      this.draw();
    } catch (e) {
      Neon.handleError(e);
    }
  }

  public draw() {
    render(template(this), this);
  }

  public async save(_?: any) {
    await this.service.update(this.config);
    this.draw();
  }
}
