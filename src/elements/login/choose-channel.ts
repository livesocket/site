import { render, html } from 'lit-html';
import { Element, NeonElement } from '../../lib';
import { channels, channel } from '../../helpers/channel';
import { admin } from '../../helpers/admin';

// prettier-ignore
const template = (e: ChooseChannel) => html`
  <h1>Choose channel to moderate</h1>
  <channel-grid>
    ${admin() ? html`<admin-button @click=${() => Neon.navigate('admin-home')}><channel-button-overlay><name-area>admin area</name-area></channel-button-overlay></admin-button>` : ''}
    ${channels().map(channel => html`<channel-button channel="${channel}" @click=${e.setChannel.bind(e, channel)}></channel-button>`)}
  </button-group>
`;

@Element('choose-channel')
export class ChooseChannel extends NeonElement {
  public connectedCallback() {
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }

  public setChannel(name: string) {
    channel(name);
    Neon.navigate('home-');
  }
}
