import { Element, NeonElement, Attribute } from '../../lib';
import { LoginService } from '../../services/LoginService';

@Element('login-token')
export class Login extends NeonElement {
  @Attribute()
  public access_token: string;
  @Attribute()
  public state: string;
  private service = Neon.get<LoginService>('LoginService');

  public async connectedCallback() {
    try {
      await this.service.login(this.access_token, this.state);
    } catch (e) {
      Neon.handleError(e);
    }
  }
}
