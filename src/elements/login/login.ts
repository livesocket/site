import { render, html } from 'lit-html';
import { Element, NeonElement } from '../../lib';
import { LoginService } from '../../services/LoginService';

const template = (e: Login) => html`
  <h1>Login with your Twitch account</h1>
  <p>Clicking login will direct you to twitch</p>
  <button type="button" @click=${e.login.bind(e)}>Login</button>
`;

@Element('login-')
export class Login extends NeonElement {
  private service = Neon.get<LoginService>('LoginService');

  public connectedCallback() {
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }

  public async login() {
    if (!!this.service.token) {
      try {
        if (!!(await this.service.validate())) {
          await this.service.login();
        }
      } catch (e) {
        Neon.handleError(e);
      }
    } else {
      await this.service.authorize();
    }
  }
}
