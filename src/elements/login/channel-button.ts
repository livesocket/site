import { render, html } from 'lit-html';
import { Element, NeonElement, Attribute } from '../../lib';

const template = (e: ChannelButton) => html`
  <channel-button-overlay>
    <name-area>${e.channel}</name-area>
  </channel-button-overlay>
`;

@Element('channel-button')
export class ChannelButton extends NeonElement {
  @Attribute()
  public channel: string;

  public connectedCallback() {
    this.style.backgroundImage = "url('/assets/code-background.png')";
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }
}
