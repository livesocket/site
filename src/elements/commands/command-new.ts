import { render, html } from 'lit-html';
import { Element, NeonElement, onEvent } from '../../lib';
import { Command } from '../../models/Command';
import { CommandService } from '../../services/CommandService';

const template = (e: CommandNew) => html`
  <form
    @submit=${(event: Event) => {
      event.preventDefault();
      e.submit();
    }}
  >
    <text-input placeholder="Name" @change=${onEvent(name => (e.command.name = name))}> </text-input>
    <text-input placeholder="Response" @change=${onEvent(response => (e.command.response = response))}></text-input>
    <text-input
      placeholder="Description"
      @change=${onEvent(description => (e.command.description = description))}
    ></text-input>
    <number-input placeholder="Cooldown" @change=${onEvent(cooldown => (e.command.cooldown = cooldown))}></number-input>
    <button-group>
      <button type="submit">Create Command</button>
      <button
        type="button"
        @click=${() => {
          e.command = {} as Command;
          e.trigger('done');
        }}
      >
        Cancel
      </button>
    </button-group>
  </form>
`;

@Element('command-new')
export class CommandNew extends NeonElement {
  public command = {} as Command;
  private service = Neon.get<CommandService>('CommandService');

  public connectedCallback() {
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }

  public async submit() {
    try {
      this.validate();
      await this.service.create(this.command);
      this.trigger('done');
    } catch (e) {
      Neon.handleError(e);
    }
  }

  private validate() {
    if (!this.command.name) {
      throw new Error('Command name is required.');
    }
    if (!/^[a-zA-Z0-9]*$/.test(this.command.name)) {
      throw new Error('Command name must not contain any special characters.');
    }
    if (!this.command.response) {
      throw new Error('Command response is required.');
    }
  }
}
