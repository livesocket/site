import { render, html } from 'lit-html';
import { Element, NeonElement } from '../../lib';
import { Command } from '../../models/Command';
import { CommandService } from '../../services/CommandService';

// prettier-ignore
const template = (e: CommandList) => html`
  <hint-text>Click on a field to edit</hint-text>
	${e.commands.map(command => html`<command-item .item=${command}></command-item>`)}
`;

@Element('command-list')
export class CommandList extends NeonElement {
  public commands: Command[];
  private service: CommandService = Neon.get('CommandService');

  public async connectedCallback() {
    this.refresh();
  }

  public draw() {
    render(template(this), this);
  }

  public async refresh() {
    this.commands = await this.service.get();
    this.draw();
  }
}
