import { render, html } from 'lit-html';
import { Element, NeonElement, Redraw, Attribute } from '../../lib';

const template = (e: TextInput) => html`
  <input type="text" .value=${e.value || ''} @change=${e.change.bind(e)} placeholder=${e.placeholder} />
`;

@Element('text-input')
export class TextInput extends NeonElement {
  @Redraw()
  public value: string;
  @Attribute()
  public placeholder: string;

  public connectedCallback() {
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }

  public change(event: Event) {
    event.stopPropagation();
    // Update to new value, validate, and trigger a change event passing the new value
    this.value = (event.target as HTMLInputElement).value;
    this.trigger('change', this.value);
  }
}
