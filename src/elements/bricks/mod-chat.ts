import { render, html } from 'lit-html';
import { Element, NeonElement, Redraw } from '../../lib';
import { ModChatService } from '../../services/ModChatService';
import { ModChatMessage } from '../../models/ModChatMessage';

const template = (e: ModChat) => html`
  <chat-header>Moderator Chat</chat-header>
  <chat-box>
    ${e.messages.map(
      x =>
        html`
          <chat-message .message=${x}></chat-message>
        `
    )}
  </chat-box>
  <chat-message-box>
    <textarea
      placeholder="Enter a message to chat"
      rows="3"
      @input=${(event: Event) => (e.message = event.target['value'])}
      @keyup=${(event: KeyboardEvent) => (event.key === 'Enter' ? e.send() : void 0)}
    ></textarea>
    <button type="button" @click=${() => e.send()}>Send</button>
  </chat-message-box>
`;

@Element('mod-chat')
export class ModChat extends NeonElement {
  @Redraw()
  public messages: ModChatMessage[] = [];
  public get message(): string {
    return this.querySelector<HTMLTextAreaElement>('chat-message-box textarea').value;
  }
  public set message(value: string) {
    this.querySelector<HTMLTextAreaElement>('chat-message-box textarea').value = value;
  }
  private service = Neon.get<ModChatService>('ModChatService');

  public async connectedCallback() {
    try {
      this.messages = (await this.service.history()).reverse();
      this.service.onMessage(message => {
        this.messages.push(message);
        // tslint:disable-next-line
        console.log('messages', this.messages);
        this.prune();
        this.draw();
      });
    } catch (e) {
      Neon.handleError(e);
    }
  }

  public draw() {
    render(template(this), this);
    this.scrollDown();
  }

  public scrollDown() {
    this.querySelector('chat-box').scroll(0, Number.MAX_SAFE_INTEGER);
  }

  public prune() {
    const start = this.messages.length - 100;
    this.messages = this.messages.slice(start < 0 ? 0 : start, this.messages.length);
    // tslint:disable-next-line
    console.log('pruned', this.messages);
  }

  public async send() {
    const message = this.message;
    if (!!message && !!message.replace('\n', '')) {
      await this.service.send(this.message.replace('\n', ''));
      this.message = '';
    }
  }
}
