import { render, html } from 'lit-html';
import { Element, NeonElement, Redraw } from '../../lib';
import { ModChatMessage } from '../../models/ModChatMessage';

const template = (e: ChatMessage) => html`
  <username>${e.message.name}</username> ${e.message.message}
`;

@Element('chat-message')
export class ChatMessage extends NeonElement {
  @Redraw()
  public message: ModChatMessage;

  public connectedCallback() {
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }
}
