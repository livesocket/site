const octicons = require('octicons');
import { render, html } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { Element, NeonElement, Redraw, Attribute } from '../../lib';

const template = (e: TextEdit) => html`
  ${e.editing
    ? html`
        <input
          type="text"
          .value=${e.value}
          @input=${(event: Event) => (e.value = (event.target as any).value)}
          @keyup=${(event: KeyboardEvent) => (event.key === 'Enter' ? e.commit() : void 0)}
        />
        <button
          type="button"
          @click=${(event: Event) => {
            event.stopPropagation();
            e.commit();
          }}
        >
          ${unsafeHTML(octicons['check'].toSVG())}
        </button>
      `
    : e.value || e.hint || ''}
`;

@Element('text-edit')
export class TextEdit extends NeonElement {
  public value: string;

  @Attribute()
  public hint: string;

  @Redraw()
  public editing: boolean = false;

  public connectedCallback() {
    this.addEventListener('click', (event: Event) => {
      if (!this.editing && (event.target as HTMLButtonElement).tagName !== 'BUTTON') {
        this.editing = true;
      }
    });
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }

  public commit() {
    this.editing = false;
    this.trigger('commit', this.value);
    this.draw();
  }
}
