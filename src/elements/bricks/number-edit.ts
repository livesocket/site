const octicons = require('octicons');
import { render, html } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { Element, NeonElement, Attribute, Redraw } from '../../lib';

const template = (e: NumberEdit) => html`
  ${e.editing
    ? html`
        <input
          type="number"
          value=${e.value}
          min=${e.min}
          max=${e.max}
          step=${e.step}
          @input=${(event: Event) => (e.value = +event.target['value'])}
          @keyup=${(event: KeyboardEvent) => (event.key === 'Enter' ? e.commit() : void 0)}
        />
        <button
          type="button"
          @click=${(event: Event) => {
            event.stopPropagation();
            e.commit();
          }}
        >
          ${unsafeHTML(octicons['check'].toSVG())}
        </button>
      `
    : e.value || e.hint || ''}
`;

@Element('number-edit')
export class NumberEdit extends NeonElement {
  public value: number;
  @Attribute()
  public label: string;
  @Attribute()
  public min: string;
  @Attribute()
  public max: string;
  @Attribute()
  public step: string;
  @Attribute()
  public hint: string;
  @Redraw()
  public editing: boolean = false;

  public connectedCallback() {
    this.addEventListener('click', (event: Event) => {
      if (!this.editing && (event.target as HTMLButtonElement).tagName !== 'BUTTON') {
        this.editing = true;
      }
    });
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }

  public commit() {
    this.editing = false;
    this.trigger('commit', this.value);
  }
}
