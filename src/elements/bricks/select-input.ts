const octicons = require('octicons');
import { html, render } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { Element, Attribute, Redraw, NeonElement } from '../../lib';

// prettier-ignore
const template = (e: SelectInput) => html`
	<i @click=${(event: Event) => e.openList(event)}>
		${unsafeHTML(octicons['chevron-down'].toSVG())}
	</i>
  <input-area>
    <select-display @click="${(event: Event) => e.openList(event)}"> ${!!e.value ? e.options.get(e.value) : e.placeholder || ''} </select-display>
    ${e.open
      ? html`<select-options>
          ${Array.from(e.options).map(([key, value]) => html`<select-option @click="${(event: Event) => e.select(key, event)}">${value}</select-option>`)}
        </select-options>`
      : ''
    }
  </input-area>
`;

@Element('select-input')
export class SelectInput extends NeonElement {
  @Attribute()
  public placeholder: string;

  @Redraw()
  public options: Map<string | number, string>;

  @Redraw()
  public open: boolean = false;

  @Redraw()
  public value: string | number;

  private documentClick = (() => (this.open = false)).bind(this);

  public connectedCallback() {
    document.addEventListener('click', this.documentClick);
  }

  public disconnectedCallback() {
    document.removeEventListener('click', this.documentClick);
  }

  public draw() {
    render(template(this), this);
  }

  public select(key: string | number, event: Event) {
    event.stopPropagation();
    this.open = false;
    this.value = key;
    this.trigger('change', this.value);
  }

  public openList(event: Event) {
    event.stopPropagation();
    this.open = true;
  }

  public close(): void {
    this.open = false;
  }
}
