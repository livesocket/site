const octicons = require('octicons');
import { render, html } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { Element, NeonElement, Redraw } from '../../lib';

// prettier-ignore
const template = (e: CheckBox) => html`
	<check-box-outline>
		${!!e.value ? unsafeHTML(octicons['check'].toSVG()) : html`<svg version="1.1" width="12" height="16" viewBox="0 0 12 16"></svg>`}
	</check-box-outline>
`;

@Element('check-box')
export class CheckBox extends NeonElement {
  @Redraw()
  public value: boolean = false;

  private _onClick = this.onClick.bind(this);
  public connectedCallback() {
    this.addEventListener('click', this._onClick);
    this.draw();
  }

  public onClick() {
    this.trigger('change', (this.value = !this.value));
  }

  public draw() {
    render(template(this), this);
  }
}
