import { render, html } from 'lit-html';
import { Element, NeonElement } from '../../lib';

const template = (e: ModColumn) => html`
  <tab-area>
    <tab class=${e.activeTab === void 0 ? 'active' : ''} @click=${() => e.closePanels()}>Home</tab>
    <tab class=${e.activeTab === 'commands' ? 'active' : ''} @click=${() => e.toggleTab('commands')}>Commands</tab>
    <tab class=${e.activeTab === 'config' ? 'active' : ''} @click=${() => e.toggleTab('config')}>Config</tab>
  </tab-area>
  <mod-chat></mod-chat>
  ${!!e.activeTab
    ? html`
        <mod-panel></mod-panel>
      `
    : ''}
`;

@Element('mod-column')
export class ModColumn extends NeonElement {
  public activeTab: string;

  public connectedCallback() {
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }

  public closePanels() {
    this.activeTab = void 0;
    this.draw();
  }

  public toggleTab(name: string) {
    if (this.activeTab === name) {
      this.activeTab = void 0;
    } else {
      this.activeTab = name;
    }
    this.draw();
  }
}
