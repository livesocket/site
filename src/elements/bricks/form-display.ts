import { render, html } from 'lit-html';
import { Element, NeonElement, Attribute } from '../../lib';

const template = (e: FormDisplay) => html`
  <form-label>${e.label}</form-label>
  <form-value>${e.value}</form-value>
`;

@Element('form-display')
export class FormDisplay extends NeonElement {
  @Attribute()
  public label: string;
  @Attribute()
  public value: string;

  public connectedCallback() {
    render(template(this), this);
  }
}
