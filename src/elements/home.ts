import { render, html } from 'lit-html';
import { Element, NeonElement } from '../lib';

// prettier-ignore
const template = (e: Home) => html`
  <mod-column></mod-column>
  <center-column>
  </center-column>
`;

@Element('home-')
export class Home extends NeonElement {
  public connectedCallback() {
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }
}
