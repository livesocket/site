// admin
import './admin';

// bricks
import './bricks/category-box';
import './bricks/chat-message';
import './bricks/check-box';
import './bricks/form-display';
import './bricks/mod-chat';
import './bricks/mod-column';
import './bricks/number-edit';
import './bricks/select-input';
import './bricks/text-edit';
import './bricks/text-input';
import './bricks/toolbar';

// commands
import './commands/command-home';
import './commands/command-list';
import './commands/command-item';
import './commands/command-new';

// config
import './config/config-home';
import './config/fight-options';
import './config/roll-options';
import './config/url-scan-options';

// login
import './login/channel-button';
import './login/choose-channel';
import './login/login-token';
import './login/login';

import './home';
