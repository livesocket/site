import { AlertType } from './AlertType';
import { AlertContainer } from './dom/alert-container';

function alert(type: AlertType, message: string, title?: string, timeout?: number): string {
  Neon.get<AlertContainer>('AlertContainer').alert(type, message, title, timeout);
  return message;
}

export function success(message: string, title?: string): string {
  return alert(AlertType.Success, message, <string>title, 2500);
}

export function error(message: string, title?: string): string {
  return alert(AlertType.Error, message, title);
}

export function warn(message: string, title?: string): string {
  return alert(AlertType.Warn, message, title, 2500);
}

export function info(message: string, title?: string): string {
  return alert(AlertType.Info, message, title, 2500);
}
