import { Values, NeonElement } from '../elements/NeonElement';

export type Getter = (value: any) => any;
export type Setter = (value: any) => void;
export interface PropertySettings {
  value: any;
  gets: Getter[];
  sets: Setter[];
}

export function registerGetSetProperty(instance: any, prop: string) {
  if (!instance[Values].has(prop)) {
    const settings = { value: instance[prop], gets: [], sets: [] } as PropertySettings;
    Object.defineProperty(instance, prop, {
      get() {
        return settings.gets.reduce((result, get) => get(result), settings.value);
      },
      set(value: any) {
        settings.value = value;
        settings.sets.forEach(set => set(value));
      }
    });
    instance[Values].set(prop, settings);
  }
}

export function appendGet(instance: any, prop: string, ...gets: Getter[]) {
  const settings = instance[Values].get(prop);
  settings.gets.push(...gets);
}

export function appendSet(instance: any, prop: string, ...sets: Setter[]) {
  const settings = instance[Values].get(prop);
  settings.sets.push(...sets);
}

export function getDecoratorData<T extends string[] | Map<string, string> | Map<string, string[]>>(
  instance: NeonElement,
  type: symbol
): T {
  let result: T;
  const c = instance.constructor;
  const s = Object.getPrototypeOf(c);
  if (s !== c) {
    if (s[type] && s[type][s.name]) {
      result = extractAppend<T>(s[type][s.name], result);
    }
  }
  if (c[type] && c[type][c.name]) {
    result = extractAppend<T>(c[type][c.name], result);
  }
  return result;
}

export function ensureDecoratorData<T>(c: Function, type: symbol, ifEmpty: any): T {
  if (!c[type]) {
    c[type] = { [c.name]: ifEmpty };
  }
  if (!c[type][c.name]) {
    c[type][c.name] = ifEmpty;
  }
  return c[type][c.name];
}

function extractAppend<T extends string[] | Map<string, string> | Map<string, string[]>>(
  item: T,
  collection: T | void
): T {
  if (Array.isArray(item)) {
    let result: string[];
    result = (collection as string[]) || ([] as string[]);
    result = result.concat(item);
    return result as T;
  }
  if (item instanceof Map) {
    let result: Map<string, string | string[]>;
    result = (collection as Map<string, string | string[]>) || new Map();
    item.forEach((value, key) => result.set(key, value));
    return result as T;
  }
}
