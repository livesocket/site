import { NeonElement } from '../elements/NeonElement';
import { Element } from '../decorators/Element';
import { AlertItem } from './alert-item';
import { AlertType } from '../AlertType';
@Element('alert-container')
export class AlertContainer extends NeonElement {
  constructor() {
    super();
    Neon.bind('AlertContainer').toConstantValue(this);
  }

  public alert(type: AlertType, message: string, title?: string, timeout?: number): void {
    const alertItem = document.createElement('alert-item') as AlertItem;
    alertItem.type = type;
    alertItem.message = message;
    alertItem.title = title;
    // 0 is false, so set timeout to void 0 if number is 0
    alertItem.timeout = !!timeout ? timeout : void 0;
    this.appendChild(alertItem);
  }
}
