import { Element } from '../decorators/Element';
import { NeonElement } from '../elements/NeonElement';

@Element('router-outlet')
class RouterOutlet extends NeonElement {
  public handler = this.navigate.bind(this);
  public current: HTMLElement;
  private body: HTMLBodyElement = document.querySelector('body');

  constructor() {
    super();
    if (!!location.hash) {
      this.navigate();
    }
    window.addEventListener('hashchange', this.handler);
  }

  public disconnectedCallback() {
    window.removeEventListener('hashchange', this.handler);
  }

  private extract(): [string, { [key: string]: string }] {
    if (location.hash.startsWith('#access_token=')) {
      location.hash = `#login-token?${location.hash.slice(1)}`;
      return;
    }
    const hash = location.hash.slice(1).split('?');
    const element = hash[0].indexOf('=') > -1 ? hash[0].split('=')[0] : hash[0];
    const attrs = !!hash[1]
      ? hash[1].split('&').reduce((result, item) => {
          const items = item.split('=');
          result[items[0]] = decodeURIComponent(items[1]);
          return result;
        }, {})
      : void 0;
    return [element, attrs];
  }

  private navigate() {
    if (!!this.current) {
      this.current.remove();
      this.body.classList.remove(this.current.tagName.toLowerCase());
    }
    const parts = this.extract();
    if (!parts) {
      return;
    }
    const [element, attrs] = parts;
    if (!!element) {
      this.current = document.createElement(element);
      if (!!attrs) {
        Object.keys(attrs).forEach(attr => this.current.setAttribute(attr, attrs[attr]));
      }
      this.appendChild(this.current);
      this.body.classList.add(this.current.tagName.toLowerCase());
    }
  }
}
