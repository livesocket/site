import { decorate } from './decorate';

/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

export const Values = Symbol('Values');

export class NeonElement extends HTMLElement {
  constructor() {
    super();
    decorate(this);
  }

  public trigger(event: string, data?: any) {
    this.dispatchEvent(new CustomEvent(event, { detail: data }));
  }
}
