import { NeonElement } from '../elements/NeonElement';

interface PopupContainer<T, K> {
  complete(result?: T): void;
  cancel(result?: K): void;
}

export class PopupElement<CompleteResult = any, CancelResult = any> extends NeonElement {
  public complete(result?: CompleteResult) {
    const container = this.findContainer();
    if (!!container) {
      container.complete(result);
    }
  }

  public cancel(result?: CancelResult) {
    const container = this.findContainer();
    if (!!container) {
      container.cancel(result);
    }
  }

  private findContainer(): PopupContainer<CompleteResult, CancelResult> | void {
    // Search up the tree for a parent of `<popup-container>` and return it
    // If one is not found before hitting `<body>` return void 0
    let position: HTMLElement = this;
    while (true) {
      if (!position) {
        return void 0;
      }
      position = position.parentElement;
      if (position.tagName === 'POPUP-CONTAINER') {
        return position as any;
      }
      if (position instanceof HTMLBodyElement) {
        return void 0;
      }
    }
  }
}
