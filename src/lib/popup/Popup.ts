import { PopupElement, PopupOptions, PopupResult } from '../index';
import { PopupContainer } from '../dom/popup-container';

export function show<EType extends PopupElement, CompleteResult = any, CancelResult = any>(
  element: string,
  args?: { [T in keyof EType]?: EType[T] },
  options: PopupOptions = {}
): PopupResult<CompleteResult, CancelResult> {
  return Neon.get<PopupContainer>('PopupContainer').show(element, args, options);
}

export function close(): void {
  Neon.get<PopupContainer>('PopupContainer').close();
}
