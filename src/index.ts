import 'reflect-metadata';
import './lib/Neon';
import './lib/dom';
import './services';
import './elements';
if (!(location.hash.startsWith('#access_token') || location.hash.startsWith('#login-token'))) {
  Neon.navigate('login-');
}
