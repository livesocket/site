let active: string;
let available: string[] = ['snarechops', 'deathbypeaches', 'otherstream', 'somethingrandom'];
export function channel(value?: string): string {
  if (!!value) {
    active = value;
  }
  return active;
}

export function channels(values?: string[]): string[] {
  if (!!values) {
    available = values;
  }
  return available || [];
}
