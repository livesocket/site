let isAdmin: boolean;
export function admin(value?: boolean): boolean {
  if (!!value) {
    isAdmin = value;
  }
  return isAdmin;
}
