const path = require('path');

module.exports = {
  mode: 'development',

  entry: './dist/client-test/index.test.js',

  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'tests.js'
  },
  externals: ['autobahn']
};
