#-------------------------#
#    Base Dependencies    #
#-------------------------#
FROM registry.gitlab.com/livesocket/site-test-container:latest as base

#-------------------------#
#        Builder          #
#-------------------------#
FROM base AS builder
ENV DOCKERIZE_VERSION v0.6.1
WORKDIR /usr/src
COPY . .
RUN npm install \
	&& echo "export const client_id = '5u09wyn5ytiatfn51k20p5vd8uak3v';" > ./src/config.ts \
	&& echo "export const redirect_uri = 'https://bot.snarechops.net';" >> ./src/config.ts \
	&& cat ./src/config.ts \
	&& npm test

#-------------------------#
#      Release Node       #
#-------------------------#

FROM pierrezemb/gostatic as release
COPY --from=builder /usr/src/public/ /srv/http/
ENTRYPOINT ["/goStatic"]