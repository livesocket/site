export declare class ModChatMessage {
    channel: string;
    message: string;
    id: number;
    name: string;
    timestamp?: Date;
    constructor(channel: string, message: string);
}
