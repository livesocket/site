export declare enum PermissionLevel {
    ALL = 0,
    SUB = 1,
    VIP = 2,
    MOD = 3,
    BROADCASTER = 4
}
export declare const PermissionLevelMap: Map<number, string>;
