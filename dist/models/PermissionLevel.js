export var PermissionLevel;
(function (PermissionLevel) {
    PermissionLevel[PermissionLevel["ALL"] = 0] = "ALL";
    PermissionLevel[PermissionLevel["SUB"] = 1] = "SUB";
    PermissionLevel[PermissionLevel["VIP"] = 2] = "VIP";
    PermissionLevel[PermissionLevel["MOD"] = 3] = "MOD";
    PermissionLevel[PermissionLevel["BROADCASTER"] = 4] = "BROADCASTER";
})(PermissionLevel || (PermissionLevel = {}));
export const PermissionLevelMap = new Map([
    [0, 'All'],
    [1, 'Subscriber'],
    [2, 'VIP'],
    [3, 'Moderator'],
    [4, 'Broadcaster']
]);
//# sourceMappingURL=PermissionLevel.js.map