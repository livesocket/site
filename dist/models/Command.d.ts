export interface Command {
    channel: string;
    name: string;
    response: string;
    enabled: boolean;
    restricted: boolean;
    cooldown: number;
    description?: string;
    scheduled: boolean;
    schedule: number;
    createdBy: string;
    createdAt: Date;
}
