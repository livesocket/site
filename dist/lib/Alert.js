import { AlertType } from './AlertType';
function alert(type, message, title, timeout) {
    Neon.get('AlertContainer').alert(type, message, title, timeout);
    return message;
}
export function success(message, title) {
    return alert(AlertType.Success, message, title, 2500);
}
export function error(message, title) {
    return alert(AlertType.Error, message, title);
}
export function warn(message, title) {
    return alert(AlertType.Warn, message, title, 2500);
}
export function info(message, title) {
    return alert(AlertType.Info, message, title, 2500);
}
//# sourceMappingURL=Alert.js.map