export * from './decorators';
export * from './elements';
import { interfaces, Container } from 'inversify';
import { PopupOptions } from './popup/PopupOptions';
import { PopupResult } from './popup/PopupResult';
import { PopupElement } from './popup/PopupElement';
export { Attribute } from './decorators/Attribute';
export { Element } from './decorators/Element';
export { Redraw } from './decorators/Redraw';
export { OnSet } from './decorators/OnSet';
export { Class } from './decorators/Class';
export { PopupElement } from './popup/PopupElement';
export { PopupOptions } from './popup/PopupOptions';
export { PopupResult } from './popup/PopupResult';
export { NeonElement } from './elements/NeonElement';
export { MenuItem, MenuGuard, MenuCallback } from './MenuItem';
export { NavigationGuard, NavigationArgs } from './types';
export { onEvent } from './helpers/onEvent';
export { contents } from './helpers/contents';
declare global {
    interface PubSub<T> {
        publish(data?: T): void;
        subscribe(handler: (data: T) => any): {
            unsubscribe(): void;
        };
    }
    interface MessageTokens {
        default: string;
        [key: string]: string;
    }
    interface Neon {
        alert: {
            success(message: string, title?: string): string;
            error(message: string, title?: string): string;
            warn(message: string, title?: string): string;
            info(message: string, title?: string): string;
        };
        logger: {
            debug(...args: any[]): any;
            info(...args: any[]): any;
            warn(...args: any[]): any;
            error(...args: any[]): any;
        };
        popup: {
            show<EType extends PopupElement, CompleteResult = any, CancelResult = any>(element: string, args?: {
                [T in keyof EType]?: EType[T];
            }, options?: PopupOptions): PopupResult<CompleteResult, CancelResult>;
            close(): void;
        };
        container: Container;
        get<T = any>(token: string, name?: string): T;
        bind<T = any>(token: string): interfaces.BindingToSyntax<T>;
        bound(token: string, name?: string): boolean;
        unbind(token: string): void;
        unbindAll(): void;
        navigate(element: string, attributes?: {
            [key: string]: string | number | boolean;
        }): void;
        pubSub<T>(name: string): PubSub<T>;
        emit(event: string, data: any): void;
        listen(event: string, handler: (detail: any) => any): {
            unsubscribe(): void;
        };
        once(event: string, handler: (detail: any) => any): {
            unsubscribe(): void;
        };
        handleError(error: string | Error, messages?: string | MessageTokens): void;
        logs(): void;
        href(url: string): void;
        back(): void;
    }
    const Neon: Neon;
    interface Window {
        Neon: Neon;
    }
}
