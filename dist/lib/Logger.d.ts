export declare enum LogLevel {
    DEBUG = 0,
    INFO = 1,
    WARN = 2,
    ERROR = 3
}
export interface LogItem {
    level: LogLevel;
    items: any[];
}
export declare function debug(...args: any[]): any;
export declare function info(...args: any[]): any;
export declare function warn(...args: any[]): any;
export declare function error(...args: any[]): any;
export declare function toConsole(items?: LogItem[]): void;
