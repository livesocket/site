import { NeonElement } from '../elements/NeonElement';
export declare type Getter = (value: any) => any;
export declare type Setter = (value: any) => void;
export interface PropertySettings {
    value: any;
    gets: Getter[];
    sets: Setter[];
}
export declare function registerGetSetProperty(instance: any, prop: string): void;
export declare function appendGet(instance: any, prop: string, ...gets: Getter[]): void;
export declare function appendSet(instance: any, prop: string, ...sets: Setter[]): void;
export declare function getDecoratorData<T extends string[] | Map<string, string> | Map<string, string[]>>(instance: NeonElement, type: symbol): T;
export declare function ensureDecoratorData<T>(c: Function, type: symbol, ifEmpty: any): T;
