import { NeonElement } from '../elements/NeonElement';
export declare const Attributes: unique symbol;
export declare function Attribute(name?: string): PropertyDecorator;
export declare function registerAttributes(instance: NeonElement): void;
