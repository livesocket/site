import { Values } from '../elements/NeonElement';
export function registerGetSetProperty(instance, prop) {
    if (!instance[Values].has(prop)) {
        const settings = { value: instance[prop], gets: [], sets: [] };
        Object.defineProperty(instance, prop, {
            get() {
                return settings.gets.reduce((result, get) => get(result), settings.value);
            },
            set(value) {
                settings.value = value;
                settings.sets.forEach(set => set(value));
            }
        });
        instance[Values].set(prop, settings);
    }
}
export function appendGet(instance, prop, ...gets) {
    const settings = instance[Values].get(prop);
    settings.gets.push(...gets);
}
export function appendSet(instance, prop, ...sets) {
    const settings = instance[Values].get(prop);
    settings.sets.push(...sets);
}
export function getDecoratorData(instance, type) {
    let result;
    const c = instance.constructor;
    const s = Object.getPrototypeOf(c);
    if (s !== c) {
        if (s[type] && s[type][s.name]) {
            result = extractAppend(s[type][s.name], result);
        }
    }
    if (c[type] && c[type][c.name]) {
        result = extractAppend(c[type][c.name], result);
    }
    return result;
}
export function ensureDecoratorData(c, type, ifEmpty) {
    if (!c[type]) {
        c[type] = { [c.name]: ifEmpty };
    }
    if (!c[type][c.name]) {
        c[type][c.name] = ifEmpty;
    }
    return c[type][c.name];
}
function extractAppend(item, collection) {
    if (Array.isArray(item)) {
        let result;
        result = collection || [];
        result = result.concat(item);
        return result;
    }
    if (item instanceof Map) {
        let result;
        result = collection || new Map();
        item.forEach((value, key) => result.set(key, value));
        return result;
    }
}
//# sourceMappingURL=helpers.js.map