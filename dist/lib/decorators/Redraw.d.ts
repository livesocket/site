import { NeonElement } from '../elements/NeonElement';
export declare const Redraws: unique symbol;
export declare function Redraw(): PropertyDecorator;
export declare function registerRedraws(instance: NeonElement): void;
