import { registerGetSetProperty, appendGet, appendSet, getDecoratorData, ensureDecoratorData } from './helpers';
export const Classes = Symbol('Classes');
export function Class(name) {
    return (target, prop) => {
        const data = ensureDecoratorData(target.constructor, Classes, new Map());
        if (data.has(prop)) {
            throw new Error('A property can only be linked to one CSS class.');
        }
        data.set(prop, name);
    };
}
export function registerClasses(instance) {
    const classes = getDecoratorData(instance, Classes);
    if (!classes) {
        return;
    }
    [...classes.entries()].forEach(([prop, name]) => {
        registerGetSetProperty(instance, prop);
        appendGet(instance, prop, () => instance.classList.contains(name || prop));
        appendSet(instance, prop, (value) => value ? instance.classList.add(name || prop) : instance.classList.remove(name || prop));
    });
}
//# sourceMappingURL=Class.js.map