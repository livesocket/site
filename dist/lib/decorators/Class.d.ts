import { NeonElement } from '../elements/NeonElement';
export declare const Classes: unique symbol;
export declare function Class(name?: string): PropertyDecorator;
export declare function registerClasses(instance: NeonElement): void;
