import { registerGetSetProperty, appendSet, getDecoratorData, ensureDecoratorData } from './helpers';
export const Redraws = Symbol('Redraws');
export function Redraw() {
    return (target, prop) => {
        const data = ensureDecoratorData(target.constructor, Redraws, []);
        data.push(prop);
    };
}
export function registerRedraws(instance) {
    const redraws = getDecoratorData(instance, Redraws);
    if (!redraws) {
        return;
    }
    redraws.forEach(prop => {
        registerGetSetProperty(instance, prop);
        appendSet(instance, prop, () => (!!instance['draw'] ? setTimeout(() => instance['draw'](), 0) : void 0));
    });
}
//# sourceMappingURL=Redraw.js.map