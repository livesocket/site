import { registerGetSetProperty, appendSet, getDecoratorData, ensureDecoratorData } from './helpers';
export const OnSets = Symbol('OnSets');
export function OnSet(...methods) {
    return (target, prop) => {
        const data = ensureDecoratorData(target.constructor, OnSets, new Map());
        if (data.has(prop)) {
            throw new Error("Only one @OnSet decorator can be used on a property. If you need to call more than one method use a single @OnSet decorator with multiple methods: @OnSet('method1', 'method2')");
        }
        data.set(prop, methods);
    };
}
export function registerOnSets(instance) {
    const onSets = getDecoratorData(instance, OnSets);
    if (!onSets) {
        return;
    }
    [...onSets.entries()].forEach(([prop, methods]) => {
        registerGetSetProperty(instance, prop);
        appendSet(instance, prop, ...methods.map(method => {
            return () => {
                setTimeout(() => instance[method](), 0);
            };
        }));
    });
}
//# sourceMappingURL=OnSet.js.map