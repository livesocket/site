import { NeonElement } from '../elements/NeonElement';
export declare const OnSets: unique symbol;
export declare function OnSet(...methods: string[]): PropertyDecorator;
export declare function registerOnSets(instance: NeonElement): void;
