export function Element(tag, options, guard) {
    if (typeof options === 'function') {
        guard = options;
        options = void 0;
    }
    return (target) => {
        if (!customElements.get(tag)) {
            customElements.define(tag, target, options);
        }
        if (!!guard) {
            Neon.get('NAVIGATION_GUARDS').set(tag, guard);
        }
    };
}
//# sourceMappingURL=Element.js.map