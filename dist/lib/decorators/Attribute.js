import { registerGetSetProperty, appendGet, appendSet, getDecoratorData, ensureDecoratorData } from './helpers';
export const Attributes = Symbol('Attributes');
export function Attribute(name) {
    return (target, prop) => {
        const data = ensureDecoratorData(target.constructor, Attributes, new Map());
        if (data.has(prop)) {
            throw new Error('A property can only be linked to one attribute.');
        }
        data.set(prop, name);
    };
}
export function registerAttributes(instance) {
    const attributes = getDecoratorData(instance, Attributes);
    if (!attributes) {
        return;
    }
    [...attributes.entries()].forEach(([prop, name]) => {
        registerGetSetProperty(instance, prop);
        appendGet(instance, prop, () => instance.getAttribute(name || prop));
        appendSet(instance, prop, (value) => instance.setAttribute(name || prop, value));
    });
}
//# sourceMappingURL=Attribute.js.map