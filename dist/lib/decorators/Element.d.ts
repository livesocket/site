import { NavigationGuard } from '../types';
export declare function Element(tag: string, guard?: NavigationGuard): ClassDecorator;
export declare function Element(tag: string, options?: ElementDefinitionOptions, guard?: NavigationGuard): ClassDecorator;
