var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Element } from '../decorators/Element';
import { NeonElement } from '../elements/NeonElement';
let RouterOutlet = class RouterOutlet extends NeonElement {
    constructor() {
        super();
        this.handler = this.navigate.bind(this);
        this.body = document.querySelector('body');
        if (!!location.hash) {
            this.navigate();
        }
        window.addEventListener('hashchange', this.handler);
    }
    disconnectedCallback() {
        window.removeEventListener('hashchange', this.handler);
    }
    extract() {
        if (location.hash.startsWith('#access_token=')) {
            location.hash = `#login-token?${location.hash.slice(1)}`;
            return;
        }
        const hash = location.hash.slice(1).split('?');
        const element = hash[0].indexOf('=') > -1 ? hash[0].split('=')[0] : hash[0];
        const attrs = !!hash[1]
            ? hash[1].split('&').reduce((result, item) => {
                const items = item.split('=');
                result[items[0]] = decodeURIComponent(items[1]);
                return result;
            }, {})
            : void 0;
        return [element, attrs];
    }
    navigate() {
        if (!!this.current) {
            this.current.remove();
            this.body.classList.remove(this.current.tagName.toLowerCase());
        }
        const parts = this.extract();
        if (!parts) {
            return;
        }
        const [element, attrs] = parts;
        if (!!element) {
            this.current = document.createElement(element);
            if (!!attrs) {
                Object.keys(attrs).forEach(attr => this.current.setAttribute(attr, attrs[attr]));
            }
            this.appendChild(this.current);
            this.body.classList.add(this.current.tagName.toLowerCase());
        }
    }
};
RouterOutlet = __decorate([
    Element('router-outlet'),
    __metadata("design:paramtypes", [])
], RouterOutlet);
//# sourceMappingURL=router-outlet.js.map