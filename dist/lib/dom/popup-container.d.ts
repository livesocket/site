import { NeonElement, PopupElement, PopupOptions, PopupResult } from '../index';
export declare class PopupContainer extends NeonElement {
    visible: boolean;
    options: PopupOptions;
    element: PopupElement;
    complete: (result?: any) => void;
    cancel: (result?: any) => void;
    constructor();
    draw(): void;
    show<EType extends PopupElement, CompleteResult = any, CancelResult = any>(element: string, args?: {
        [T in keyof EType]?: EType[T];
    }, options?: PopupOptions): PopupResult<CompleteResult, CancelResult>;
    backdropClick(): void;
    close(cancel?: boolean): void;
    private parseOptions;
}
