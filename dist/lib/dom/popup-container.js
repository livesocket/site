var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { render, html } from 'lit-html';
import { Element, NeonElement, PopupResult } from '../index';
const template = (e) => html `
  <popup-backdrop class=${e.visible ? 'visible' : 'none'} @click=${() => e.backdropClick()}></popup-backdrop>
  <popup-content> ${e.element} </popup-content>
`;
let PopupContainer = class PopupContainer extends NeonElement {
    constructor() {
        super();
        this.visible = false;
        Neon.bind('PopupContainer').toConstantValue(this);
    }
    draw() {
        render(template(this), this);
    }
    show(element, args = {}, options = {}) {
        this.options = this.parseOptions(options);
        return new PopupResult((completer, canceler) => {
            this.visible = true;
            this.style.display = 'block';
            this.element = document.createElement(element);
            Object.keys(args).forEach(key => (this.element[key] = args[key]));
            this.complete = result => {
                this.close(false);
                completer(result);
            };
            this.cancel = result => {
                this.close(false);
                canceler(result);
            };
            this.draw();
        });
    }
    backdropClick() {
        if (this.options.cancelOnBackgroundClick) {
            this.close();
        }
    }
    close(cancel = true) {
        this.visible = false;
        this.style.display = 'none';
        if (cancel) {
            this.cancel(void 0);
        }
        this.complete = void 0;
        this.cancel = void 0;
        this.element = void 0;
        this.draw();
    }
    parseOptions(options) {
        if (options.cancelOnBackgroundClick === void 0) {
            options.cancelOnBackgroundClick = true;
        }
        return options;
    }
};
PopupContainer = __decorate([
    Element('popup-container'),
    __metadata("design:paramtypes", [])
], PopupContainer);
export { PopupContainer };
//# sourceMappingURL=popup-container.js.map