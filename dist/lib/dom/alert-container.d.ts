import { NeonElement } from '../elements/NeonElement';
import { AlertType } from '../AlertType';
export declare class AlertContainer extends NeonElement {
    constructor();
    alert(type: AlertType, message: string, title?: string, timeout?: number): void;
}
