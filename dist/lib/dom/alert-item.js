var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { render, html } from 'lit-html';
import { AlertType } from '../AlertType';
import { Element } from '../decorators/Element';
import { NeonElement } from '../elements/NeonElement';
import { Redraw } from '../decorators/Redraw';
import { OnSet } from '../decorators/OnSet';
const template = (e) => html `
  <alert-title>${e.title}</alert-title>
  <alert-message>${e.message}</alert-message>
  <neon-icon>X</neon-icon>
`;
let AlertItem = class AlertItem extends NeonElement {
    constructor() {
        super();
        this.addEventListener('click', () => this.remove());
    }
    draw() {
        this.className = '';
        switch (this.type) {
            case AlertType.Success:
                this.classList.add('success');
                break;
            case AlertType.Error:
                this.classList.add('error');
                break;
            case AlertType.Warn:
                this.classList.add('warn');
                break;
            default:
                this.classList.add('info');
        }
        render(template(this), this);
    }
    startTimer() {
        if (!!this.timeout) {
            setTimeout(() => this.remove(), this.timeout);
        }
    }
};
__decorate([
    Redraw(),
    __metadata("design:type", String)
], AlertItem.prototype, "title", void 0);
__decorate([
    Redraw(),
    __metadata("design:type", String)
], AlertItem.prototype, "message", void 0);
__decorate([
    Redraw(),
    __metadata("design:type", Number)
], AlertItem.prototype, "type", void 0);
__decorate([
    OnSet('startTimer'),
    __metadata("design:type", Number)
], AlertItem.prototype, "timeout", void 0);
AlertItem = __decorate([
    Element('alert-item'),
    __metadata("design:paramtypes", [])
], AlertItem);
export { AlertItem };
//# sourceMappingURL=alert-item.js.map