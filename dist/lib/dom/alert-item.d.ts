import { AlertType } from '../AlertType';
import { NeonElement } from '../elements/NeonElement';
export declare class AlertItem extends NeonElement {
    title: string;
    message: string;
    type: AlertType;
    timeout: number;
    constructor();
    draw(): void;
    startTimer(): void;
}
