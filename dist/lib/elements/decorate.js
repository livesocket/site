import { Values } from './NeonElement';
import { registerRedraws } from '../decorators/Redraw';
import { registerOnSets } from '../decorators/OnSet';
import { registerAttributes } from '../decorators/Attribute';
import { registerClasses } from '../decorators/Class';
export function decorate(element) {
    element[Values] = new Map();
    registerRedraws(element);
    registerOnSets(element);
    registerAttributes(element);
    registerClasses(element);
}
//# sourceMappingURL=decorate.js.map