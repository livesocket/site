import { decorate } from './decorate';
/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */
export const Values = Symbol('Values');
export class NeonElement extends HTMLElement {
    constructor() {
        super();
        decorate(this);
    }
    trigger(event, data) {
        this.dispatchEvent(new CustomEvent(event, { detail: data }));
    }
}
//# sourceMappingURL=NeonElement.js.map