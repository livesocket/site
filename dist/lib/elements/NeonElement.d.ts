/**
 * @copyright 2018 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */
export declare const Values: unique symbol;
export declare class NeonElement extends HTMLElement {
    constructor();
    trigger(event: string, data?: any): void;
}
