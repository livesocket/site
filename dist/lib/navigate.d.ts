import { NavigationArgs } from './types';
export declare function navigate(element: string, args?: NavigationArgs): void;
