import * as EventEmitter from './EventEmitter';
export function pubSub(name) {
    return {
        publish: (data) => EventEmitter.emit(name, data),
        subscribe: (handler) => {
            return EventEmitter.subscribe(name, handler);
        }
    };
}
//# sourceMappingURL=PubSub.js.map