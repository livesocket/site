export function onEvent(handler) {
    return ({ detail }) => {
        handler(detail);
    };
}
//# sourceMappingURL=onEvent.js.map