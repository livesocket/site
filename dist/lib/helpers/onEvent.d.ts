export declare function onEvent<T = any>(handler: (data: T) => any): (event: CustomEvent) => any;
