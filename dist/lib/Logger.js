export var LogLevel;
(function (LogLevel) {
    LogLevel[LogLevel["DEBUG"] = 0] = "DEBUG";
    LogLevel[LogLevel["INFO"] = 1] = "INFO";
    LogLevel[LogLevel["WARN"] = 2] = "WARN";
    LogLevel[LogLevel["ERROR"] = 3] = "ERROR";
})(LogLevel || (LogLevel = {}));
let logs = [];
export function debug(...args) {
    return store(LogLevel.DEBUG, args);
}
export function info(...args) {
    return store(LogLevel.INFO, args);
}
export function warn(...args) {
    return store(LogLevel.WARN, args);
}
export function error(...args) {
    return store(LogLevel.ERROR, args);
}
function store(level, items) {
    const item = { level, items };
    logs.push(item);
    return items.length > 1 ? items : items[0];
}
export function toConsole(items = logs) {
    let args;
    for (const logItem of items) {
        args = logItem.items.splice(0);
        args.splice(0, 0, `${LogLevel[logItem.level]}:`);
        if (logItem.level === LogLevel.WARN) {
            console.warn.apply(console, args);
            continue;
        }
        if (logItem.level === LogLevel.ERROR) {
            console.error.apply(console, args);
            continue;
        }
        // tslint:disable-next-line
        console.log.apply(console, args);
    }
    logs = [];
}
window['logs'] = toConsole;
//# sourceMappingURL=Logger.js.map