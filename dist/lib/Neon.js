import { Container } from 'inversify';
import { error as alertError, warn as alertWarn, info as alertInfo, success as alertSuccess } from './Alert';
import { error as logError, warn as logWarn, info as logInfo, debug as logDebug, toConsole } from './Logger';
import * as EventEmitter from './EventEmitter';
import * as popup from './popup/Popup';
import { pubSub } from './PubSub';
import { handleError } from './handleError';
import { navigate } from './navigate';
const container = new Container();
function get(token, name) {
    return !!name ? container.getNamed(token, name) : container.get(token);
}
function bind(token) {
    return container.bind(token);
}
function bound(token, name) {
    return !!name ? container.isBoundNamed(token, name) : container.isBound(token);
}
function unbind(token) {
    return container.unbind(token);
}
function unbindAll() {
    return container.unbindAll();
}
function href(url) {
    location.href = url;
}
function back() {
    history.back();
}
window.Neon = {
    get,
    bind,
    bound,
    unbind,
    unbindAll,
    container,
    navigate,
    pubSub,
    alert: {
        error: alertError,
        warn: alertWarn,
        info: alertInfo,
        success: alertSuccess
    },
    logger: {
        error: logError,
        warn: logWarn,
        info: logInfo,
        debug: logDebug
    },
    popup,
    handleError,
    href,
    back,
    emit: EventEmitter.emit,
    listen: EventEmitter.subscribe,
    once: EventEmitter.once,
    logs: toConsole
};
//# sourceMappingURL=Neon.js.map