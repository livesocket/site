export declare enum AlertType {
    Success = 0,
    Error = 1,
    Warn = 2,
    Info = 3
}
