export function subscribe(event, handler) {
    function internal(e) {
        handler(e.detail);
    }
    document.addEventListener(event, internal);
    return { unsubscribe: () => document.removeEventListener(event, internal) };
}
export function emit(event, data) {
    document.dispatchEvent(new CustomEvent(event, { detail: data }));
}
export function once(event, handler) {
    function internal(e) {
        handler(e.detail);
        document.removeEventListener(event, internal);
    }
    document.addEventListener(event, internal);
    return { unsubscribe: () => document.removeEventListener(event, internal) };
}
//# sourceMappingURL=EventEmitter.js.map