export function navigate(element, args = {}) {
    if (check(element, args)) {
        const attrs = Object.keys(args)
            .map(attr => `${attr}=${encodeURIComponent(args[attr].toString())}`)
            .join('&');
        location.hash = `#${element}${!!attrs ? `?${attrs}` : ''}`;
    }
}
function check(element, args) {
    if (!Neon.bound('NAVIGATION_GUARDS')) {
        Neon.bind('NAVIGATION_GUARDS').toConstantValue(new Map());
    }
    const map = Neon.get('NAVIGATION_GUARDS');
    return map.has(element) ? map.get(element)(element, args) : true;
}
//# sourceMappingURL=navigate.js.map