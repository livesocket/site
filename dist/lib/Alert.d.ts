export declare function success(message: string, title?: string): string;
export declare function error(message: string, title?: string): string;
export declare function warn(message: string, title?: string): string;
export declare function info(message: string, title?: string): string;
