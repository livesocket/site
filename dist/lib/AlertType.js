export var AlertType;
(function (AlertType) {
    AlertType[AlertType["Success"] = 0] = "Success";
    AlertType[AlertType["Error"] = 1] = "Error";
    AlertType[AlertType["Warn"] = 2] = "Warn";
    AlertType[AlertType["Info"] = 3] = "Info";
})(AlertType || (AlertType = {}));
//# sourceMappingURL=AlertType.js.map