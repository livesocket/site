export class MenuItem {
    constructor(name, textToken, elementOrCallback, iconOrArgs, iconOrWeight, guardOrWeight, badgeOrGuard, badge) {
        this.weight = 0;
        this.name = name;
        this.textToken = textToken;
        typeof elementOrCallback === 'string' ? (this.element = elementOrCallback) : (this.callback = elementOrCallback);
        typeof iconOrArgs === 'string' ? (this.icon = iconOrArgs) : (this.args = iconOrArgs);
        typeof iconOrWeight === 'string' ? (this.icon = iconOrWeight) : (this.weight = iconOrWeight);
        typeof guardOrWeight === 'number' ? (this.weight = guardOrWeight) : (this.guard = guardOrWeight);
        typeof badgeOrGuard === 'number' ? (this.badge = badgeOrGuard) : (this.guard = badgeOrGuard);
        if (!!badge) {
            this.badge = badge;
        }
        if (this.weight === undefined) {
            this.weight = 0;
        }
    }
    handle() {
        if (!!this.element) {
            return Neon.navigate(this.element, this.args);
        }
        if (!!this.callback) {
            return this.callback(this);
        }
    }
}
//# sourceMappingURL=MenuItem.js.map