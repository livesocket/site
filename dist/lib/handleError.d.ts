export interface MessageTokens {
    default: string;
    [key: string]: string;
}
export declare function handleError(error: any, messages?: string | MessageTokens): string;
