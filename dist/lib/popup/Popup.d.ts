import { PopupElement, PopupOptions, PopupResult } from '../index';
export declare function show<EType extends PopupElement, CompleteResult = any, CancelResult = any>(element: string, args?: {
    [T in keyof EType]?: EType[T];
}, options?: PopupOptions): PopupResult<CompleteResult, CancelResult>;
export declare function close(): void;
