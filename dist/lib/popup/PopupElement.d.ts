import { NeonElement } from '../elements/NeonElement';
export declare class PopupElement<CompleteResult = any, CancelResult = any> extends NeonElement {
    complete(result?: CompleteResult): void;
    cancel(result?: CancelResult): void;
    private findContainer;
}
