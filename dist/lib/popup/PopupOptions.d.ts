export interface PopupOptions {
    cancelOnBackgroundClick?: boolean;
}
