import { NeonElement } from '../elements/NeonElement';
export class PopupElement extends NeonElement {
    complete(result) {
        const container = this.findContainer();
        if (!!container) {
            container.complete(result);
        }
    }
    cancel(result) {
        const container = this.findContainer();
        if (!!container) {
            container.cancel(result);
        }
    }
    findContainer() {
        // Search up the tree for a parent of `<popup-container>` and return it
        // If one is not found before hitting `<body>` return void 0
        let position = this;
        while (true) {
            if (!position) {
                return void 0;
            }
            position = position.parentElement;
            if (position.tagName === 'POPUP-CONTAINER') {
                return position;
            }
            if (position instanceof HTMLBodyElement) {
                return void 0;
            }
        }
    }
}
//# sourceMappingURL=PopupElement.js.map