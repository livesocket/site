export declare type PopupCompleter<T> = (result?: T) => void;
export declare type PopupCanceler<T> = (result?: T) => void;
export declare class PopupResult<CompleteResult extends any, CancelResult extends any> {
    private completeHandlers;
    private cancelHandlers;
    constructor(handlers: (completer: PopupCompleter<CompleteResult>, canceler: PopupCanceler<CancelResult>) => void);
    complete(handler: (result?: CompleteResult) => any): this;
    cancel(handler: (result?: CancelResult) => any): this;
}
