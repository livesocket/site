export function show(element, args, options = {}) {
    return Neon.get('PopupContainer').show(element, args, options);
}
export function close() {
    Neon.get('PopupContainer').close();
}
//# sourceMappingURL=Popup.js.map