export class PopupResult {
    constructor(handlers) {
        this.completeHandlers = [];
        this.cancelHandlers = [];
        const complete = (result) => {
            this.completeHandlers.forEach(handler => {
                handler(result);
            });
            this.completeHandlers = [];
        };
        const cancel = (result) => {
            this.cancelHandlers.forEach(handler => {
                handler(result);
            });
            this.cancelHandlers = [];
        };
        setTimeout(() => handlers(complete, cancel), 0);
    }
    complete(handler) {
        this.completeHandlers.push(handler);
        return this;
    }
    cancel(handler) {
        this.cancelHandlers.push(handler);
        return this;
    }
}
//# sourceMappingURL=PopupResult.js.map