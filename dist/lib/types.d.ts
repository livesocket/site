export interface NavigationArgs {
    [key: string]: string | number | boolean;
}
export declare type NavigationGuard = (element: string, args: NavigationArgs) => boolean;
