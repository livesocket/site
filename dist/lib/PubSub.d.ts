export interface PubSub<T> {
    publish(data?: T): void;
    subscribe(handler: (data: T) => any): {
        unsubscribe(): void;
    };
}
export declare function pubSub<T>(name: string): PubSub<T>;
