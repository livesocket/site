export declare function subscribe(event: string, handler: (detail: any) => any): {
    unsubscribe(): void;
};
export declare function emit(event: string, data: any): void;
export declare function once(event: string, handler: (detail: any) => any): {
    unsubscribe(): void;
};
