import { NavigationArgs } from './types';
export declare type MenuGuard = (item: MenuItem) => boolean;
export declare type MenuCallback<T> = (item: MenuItem) => T;
export declare class MenuItem<T = void> {
    name: string;
    textToken: string;
    icon?: string;
    weight?: number;
    badge?: number;
    guard?: MenuGuard;
    element?: string;
    args?: NavigationArgs;
    callback?: MenuCallback<T>;
    constructor(name: string, textToken: string, element: string, icon?: string, weight?: number, guard?: MenuGuard, badge?: number);
    constructor(name: string, textToken: string, element: string, args: NavigationArgs, icon?: string, weight?: number, guard?: MenuGuard, badge?: number);
    constructor(name: string, textToken: string, callback: MenuCallback<T>, icon?: string, weight?: number, guard?: MenuGuard, badge?: number);
    handle(): T | void;
}
