let active;
let available = ['snarechops', 'deathbypeaches', 'otherstream', 'somethingrandom'];
export function channel(value) {
    if (!!value) {
        active = value;
    }
    return active;
}
export function channels(values) {
    if (!!values) {
        available = values;
    }
    return available || [];
}
//# sourceMappingURL=channel.js.map