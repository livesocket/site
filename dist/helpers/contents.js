export function contents(node) {
    const nodes = [];
    node = node.firstChild;
    while (!!node) {
        nodes.push(node);
        node = node.nextSibling;
    }
    return nodes;
}
//# sourceMappingURL=contents.js.map