var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { injectable } from 'inversify';
import { channel } from '../helpers/channel';
let CommandService = class CommandService {
    constructor() {
        this.socket = Neon.get('Socket');
    }
    async get() {
        return this.socket.call('public.command.get', [channel()]);
    }
    async getById(name) {
        return this.socket.call('public.command.getById', [channel(), name]);
    }
    async create(command) {
        return this.socket.call('public.command.create', [Object.assign({}, command, { channel: channel() })]);
    }
    async update(command) {
        return this.socket.call('public.command.update', [command]);
    }
    async delete(name) {
        return this.socket.call('public.command.delete', [channel(), name]);
    }
    async toggleEnabled(command) {
        if (command.enabled) {
            return this.socket.call('public.command.disable', [channel(), command.name]);
        }
        else {
            return this.socket.call('public.command.enable', [channel(), command.name]);
        }
    }
    async toggleRestricted(command) {
        if (command.restricted) {
            return this.socket.call('public.command.unrestrict', [channel(), command.name]);
        }
        else {
            return this.socket.call('public.command.restrict', [channel(), command.name]);
        }
    }
    async toggleScheduled(command) {
        if (command.scheduled) {
            return this.socket.call('public.command.unschedule', [channel(), command.name]);
        }
        else {
            return this.socket.call('public.command.schedule', [channel(), command.name]);
        }
    }
};
CommandService = __decorate([
    injectable()
], CommandService);
export { CommandService };
//# sourceMappingURL=CommandService.js.map