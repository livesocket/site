export interface FightConfig {
    channel: string;
    enabled: boolean;
    cooldown: number;
}
export declare class FightService {
    private socket;
    get(): Promise<FightConfig>;
    update(config: FightConfig): Promise<FightConfig>;
}
