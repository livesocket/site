var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Connection } from 'autobahn';
import { injectable } from 'inversify';
let Socket = class Socket {
    constructor() {
        // tslint:disable-next-line
        console.log('Creating instance of Socket');
    }
    connect(token) {
        return this._connect({
            // url: `ws://${location.hostname}:8080`,
            url: `wss://${location.hostname}/ws/`,
            realm: 'chatbot',
            authmethods: ['ticket'],
            authid: 'access_token',
            onchallenge: () => token
        });
    }
    async call(procedure, args, kwargs, options) {
        return await this.session.call(procedure, args, kwargs, options);
    }
    async subscribe(topic, handler, options) {
        return await this.session.subscribe(topic, handler, options);
    }
    _connect(options) {
        return new Promise((resolve, reject) => {
            this.connection = new Connection(options);
            this.connection.onopen = (session, details) => {
                this.session = session;
                this.details = details;
                resolve(this.details);
            };
            this.connection.onclose = (reason, details) => {
                this.session = void 0;
                this.details = void 0;
                reject(new Error(reason));
                return true;
            };
            this.connection.open();
        });
    }
};
Socket = __decorate([
    injectable(),
    __metadata("design:paramtypes", [])
], Socket);
export { Socket };
//# sourceMappingURL=Socket.js.map