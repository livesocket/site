var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { injectable } from 'inversify';
import { channel } from '../helpers/channel';
let FightService = class FightService {
    constructor() {
        this.socket = Neon.get('Socket');
    }
    async get() {
        return this.socket.call('public.fight.config.getByChannel', [channel()]);
    }
    async update(config) {
        return this.socket.call('public.fight.config.update', [config]);
    }
};
FightService = __decorate([
    injectable()
], FightService);
export { FightService };
//# sourceMappingURL=FightService.js.map