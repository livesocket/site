export declare class LoginService {
    token: string;
    state: string;
    private socket;
    validate(): Promise<boolean>;
    authorize(): void;
    login(token?: string, state?: string): Promise<void>;
}
