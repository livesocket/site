import { Command } from '../models/Command';
export declare class CommandService {
    private socket;
    get(): Promise<Command[]>;
    getById(name: string): Promise<Command>;
    create(command: Command): Promise<Command>;
    update(command: Command): Promise<Command>;
    delete(name: string): Promise<void>;
    toggleEnabled(command: Command): Promise<Command>;
    toggleRestricted(command: Command): Promise<Command>;
    toggleScheduled(command: Command): Promise<Command>;
}
