var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { injectable } from 'inversify';
import { channel } from '../helpers/channel';
let ModChatService = class ModChatService {
    constructor() {
        this.onMessages = [];
        this.socket = Neon.get('Socket');
        this.socket.subscribe(`public.event.modchat.message.${channel().replace('#', '')}`, ([message]) => this.onMessages.forEach(x => x(message)));
    }
    onMessage(handler) {
        this.onMessages.push(handler);
    }
    async history(offset = 0, limit = 50) {
        const result = await this.socket.call('public.modchat.get', [], { channel: channel(), offset, limit });
        // tslint:disable-next-line
        console.log('result', result);
        return result.args || [];
    }
    async send(message) {
        return this.socket.call('public.modchat.send', [], { channel: channel(), message });
    }
};
ModChatService = __decorate([
    injectable(),
    __metadata("design:paramtypes", [])
], ModChatService);
export { ModChatService };
//# sourceMappingURL=ModChatService.js.map