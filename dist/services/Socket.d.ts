import { ISubscription, SubscribeHandler, ISubscribeOptions } from 'autobahn';
export interface ConnectionDetails {
    authid: string;
    authrole: string;
    authextra: any;
}
export declare class Socket {
    private connection;
    private session;
    private details;
    constructor();
    connect(token: string): Promise<ConnectionDetails>;
    call<T = any>(procedure: string, args?: any[], kwargs?: any, options?: any): Promise<T>;
    subscribe<T = any>(topic: string, handler: SubscribeHandler, options?: ISubscribeOptions): Promise<ISubscription>;
    private _connect;
}
