var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { injectable } from 'inversify';
import { client_id, redirect_uri } from '../config';
import { channels, channel } from '../helpers/channel';
import { admin } from '../helpers/admin';
const state = 'kjhsdf76234GFUH3947734sf';
let LoginService = class LoginService {
    constructor() {
        this.socket = Neon.get('Socket');
    }
    get token() {
        return localStorage.getItem('access_token');
    }
    set token(token) {
        if (!token) {
            localStorage.removeItem('access_token');
        }
        localStorage.setItem('access_token', token);
    }
    get state() {
        return localStorage.getItem('login_state');
    }
    set state(value) {
        if (!value) {
            localStorage.removeItem('login_state');
        }
        localStorage.setItem('login_state', value);
    }
    async validate() {
        return fetch('https://id.twitch.tv/oauth2/validate', { headers: { Authorization: `OAuth ${this.token}` } }).then(res => {
            if (!res.ok) {
                throw new Error('Invalid access token');
            }
            return res.json().then(x => x.login);
        });
    }
    authorize() {
        location.href = `https://id.twitch.tv/oauth2/authorize?client_id=${client_id}&redirect_uri=${encodeURIComponent(redirect_uri)}&response_type=token&scope=&state=${state}`;
        this.state = state;
    }
    async login(token = this.token, state = this.state) {
        if (state !== this.state) {
            throw new Error('State Mismatch');
        }
        this.token = token;
        const { authrole, authextra } = await this.socket.connect(this.token);
        channels(authextra.channels);
        if (authrole === 'admin') {
            admin(true);
            return Neon.navigate('choose-channel');
        }
        if (channels().length > 1) {
            return Neon.navigate('choose-channel');
        }
        channel(channels()[0]);
        Neon.navigate('home-');
    }
};
LoginService = __decorate([
    injectable()
], LoginService);
export { LoginService };
//# sourceMappingURL=LoginService.js.map