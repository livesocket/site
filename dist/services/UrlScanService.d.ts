import { UrlScan } from '../models/UrlScan';
export declare class UrlScanService {
    private socket;
    get(): Promise<UrlScan>;
    update(config: UrlScan): Promise<UrlScan>;
}
