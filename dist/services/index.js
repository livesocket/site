import { CommandService } from './CommandService';
import { LoginService } from './LoginService';
import { ModChatService } from './ModChatService';
import { FightService } from './FightService';
import { RollService } from './RollService';
import { UrlScanService } from './UrlScanService';
import { Socket } from './Socket';
import { PiService } from './PiService';
Neon.bind('CommandService').to(CommandService);
Neon.bind('LoginService').to(LoginService);
Neon.bind('ModChatService').to(ModChatService);
Neon.bind('FightService').to(FightService);
Neon.bind('RollService').to(RollService);
Neon.bind('UrlScanService').to(UrlScanService);
Neon.bind('PiService').to(PiService);
Neon.bind('Socket').toConstantValue(new Socket());
//# sourceMappingURL=index.js.map