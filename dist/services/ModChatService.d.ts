import { ModChatMessage } from '../models/ModChatMessage';
export declare type ModChatMessageHandler = (message: ModChatMessage) => any;
export declare class ModChatService {
    private onMessages;
    private socket;
    constructor();
    onMessage(handler: ModChatMessageHandler): void;
    history(offset?: number, limit?: number): Promise<ModChatMessage[]>;
    send(message: string): Promise<ModChatMessage>;
}
