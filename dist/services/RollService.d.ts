export interface RollConfig {
    channel: string;
    enabled: boolean;
    cooldown: number;
}
export declare class RollService {
    private socket;
    get(): Promise<RollConfig>;
    update(config: RollConfig): Promise<RollConfig>;
}
