var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { DateTime } from 'luxon';
import { injectable } from 'inversify';
let PiService = class PiService {
    constructor() {
        this.socket = Neon.get('Socket');
    }
    async get() {
        const result = await this.socket.call('admin.pi.get');
        return result.args.map(x => (Object.assign({}, x, { timestamp: DateTime.fromISO(x.timestamp) })));
    }
    async update(id, version) {
        return this.socket.call(`pi.${id}.update`, [version]);
    }
};
PiService = __decorate([
    injectable()
], PiService);
export { PiService };
//# sourceMappingURL=PiService.js.map