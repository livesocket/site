import { Pi } from '../models/Pi';
export declare class PiService {
    private socket;
    get(): Promise<Pi[]>;
    update(id: string, version: string): Promise<void>;
}
