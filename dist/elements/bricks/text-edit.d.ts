import { NeonElement } from '../../lib';
export declare class TextEdit extends NeonElement {
    value: string;
    hint: string;
    editing: boolean;
    connectedCallback(): void;
    draw(): void;
    commit(): void;
}
