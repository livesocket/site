var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const octicons = require('octicons');
import { render, html } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { Element, NeonElement, Redraw, Attribute } from '../../lib';
const template = (e) => html `
  ${e.editing
    ? html `
        <input
          type="text"
          .value=${e.value}
          @input=${(event) => (e.value = event.target.value)}
          @keyup=${(event) => (event.key === 'Enter' ? e.commit() : void 0)}
        />
        <button
          type="button"
          @click=${(event) => {
        event.stopPropagation();
        e.commit();
    }}
        >
          ${unsafeHTML(octicons['check'].toSVG())}
        </button>
      `
    : e.value || e.hint || ''}
`;
let TextEdit = class TextEdit extends NeonElement {
    constructor() {
        super(...arguments);
        this.editing = false;
    }
    connectedCallback() {
        this.addEventListener('click', (event) => {
            if (!this.editing && event.target.tagName !== 'BUTTON') {
                this.editing = true;
            }
        });
        this.draw();
    }
    draw() {
        render(template(this), this);
    }
    commit() {
        this.editing = false;
        this.trigger('commit', this.value);
        this.draw();
    }
};
__decorate([
    Attribute(),
    __metadata("design:type", String)
], TextEdit.prototype, "hint", void 0);
__decorate([
    Redraw(),
    __metadata("design:type", Boolean)
], TextEdit.prototype, "editing", void 0);
TextEdit = __decorate([
    Element('text-edit')
], TextEdit);
export { TextEdit };
//# sourceMappingURL=text-edit.js.map