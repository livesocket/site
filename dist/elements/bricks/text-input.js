var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { render, html } from 'lit-html';
import { Element, NeonElement, Redraw, Attribute } from '../../lib';
const template = (e) => html `
  <input type="text" .value=${e.value || ''} @change=${e.change.bind(e)} placeholder=${e.placeholder} />
`;
let TextInput = class TextInput extends NeonElement {
    connectedCallback() {
        this.draw();
    }
    draw() {
        render(template(this), this);
    }
    change(event) {
        event.stopPropagation();
        // Update to new value, validate, and trigger a change event passing the new value
        this.value = event.target.value;
        this.trigger('change', this.value);
    }
};
__decorate([
    Redraw(),
    __metadata("design:type", String)
], TextInput.prototype, "value", void 0);
__decorate([
    Attribute(),
    __metadata("design:type", String)
], TextInput.prototype, "placeholder", void 0);
TextInput = __decorate([
    Element('text-input')
], TextInput);
export { TextInput };
//# sourceMappingURL=text-input.js.map