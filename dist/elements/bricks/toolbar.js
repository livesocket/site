var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { render, html } from 'lit-html';
import { Element, NeonElement } from '../../lib';
const template = (e) => html `
  <avatar>
    <img src="/assets/avatar.png" />
  </avatar>
  <toolbar-item @click=${() => Neon.navigate('home-')}>Home</toolbar-item>
  <toolbar-item @click=${() => Neon.navigate('command-home')}>Commands</toolbar-item>
  <toolbar-item @click=${() => Neon.navigate('config-home')}>Config</toolbar-item>
`;
let Toolbar = class Toolbar extends NeonElement {
    connectedCallback() {
        Neon.bind('Toolbar').toConstantValue(this);
    }
    draw() {
        render(template(this), this);
    }
    show() {
        this.draw();
    }
    hide() {
        render(html ``, this);
    }
};
Toolbar = __decorate([
    Element('toolbar-')
], Toolbar);
export { Toolbar };
//# sourceMappingURL=toolbar.js.map