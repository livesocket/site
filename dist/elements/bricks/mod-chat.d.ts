import { NeonElement } from '../../lib';
import { ModChatMessage } from '../../models/ModChatMessage';
export declare class ModChat extends NeonElement {
    messages: ModChatMessage[];
    message: string;
    private service;
    connectedCallback(): Promise<void>;
    draw(): void;
    scrollDown(): void;
    prune(): void;
    send(): Promise<void>;
}
