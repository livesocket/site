import { NeonElement } from '../../lib';
export declare class FormDisplay extends NeonElement {
    label: string;
    value: string;
    connectedCallback(): void;
}
