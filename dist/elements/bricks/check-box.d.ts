import { NeonElement } from '../../lib';
export declare class CheckBox extends NeonElement {
    value: boolean;
    private _onClick;
    connectedCallback(): void;
    onClick(): void;
    draw(): void;
}
