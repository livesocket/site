import { NeonElement } from '../../lib';
export declare class CategoryBox extends NeonElement {
    title: string;
    contents: Node[];
    connectedCallback(): void;
    draw(): void;
}
