var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { render, html } from 'lit-html';
import { Element, NeonElement, Redraw } from '../../lib';
const template = (e) => html `
  <chat-header>Moderator Chat</chat-header>
  <chat-box>
    ${e.messages.map(x => html `
          <chat-message .message=${x}></chat-message>
        `)}
  </chat-box>
  <chat-message-box>
    <textarea
      placeholder="Enter a message to chat"
      rows="3"
      @input=${(event) => (e.message = event.target['value'])}
      @keyup=${(event) => (event.key === 'Enter' ? e.send() : void 0)}
    ></textarea>
    <button type="button" @click=${() => e.send()}>Send</button>
  </chat-message-box>
`;
let ModChat = class ModChat extends NeonElement {
    constructor() {
        super(...arguments);
        this.messages = [];
        this.service = Neon.get('ModChatService');
    }
    get message() {
        return this.querySelector('chat-message-box textarea').value;
    }
    set message(value) {
        this.querySelector('chat-message-box textarea').value = value;
    }
    async connectedCallback() {
        try {
            this.messages = (await this.service.history()).reverse();
            this.service.onMessage(message => {
                this.messages.push(message);
                // tslint:disable-next-line
                console.log('messages', this.messages);
                this.prune();
                this.draw();
            });
        }
        catch (e) {
            Neon.handleError(e);
        }
    }
    draw() {
        render(template(this), this);
        this.scrollDown();
    }
    scrollDown() {
        this.querySelector('chat-box').scroll(0, Number.MAX_SAFE_INTEGER);
    }
    prune() {
        const start = this.messages.length - 100;
        this.messages = this.messages.slice(start < 0 ? 0 : start, this.messages.length);
        // tslint:disable-next-line
        console.log('pruned', this.messages);
    }
    async send() {
        const message = this.message;
        if (!!message && !!message.replace('\n', '')) {
            await this.service.send(this.message.replace('\n', ''));
            this.message = '';
        }
    }
};
__decorate([
    Redraw(),
    __metadata("design:type", Array)
], ModChat.prototype, "messages", void 0);
ModChat = __decorate([
    Element('mod-chat')
], ModChat);
export { ModChat };
//# sourceMappingURL=mod-chat.js.map