var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const octicons = require('octicons');
import { html, render } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { Element, Attribute, Redraw, NeonElement } from '../../lib';
// prettier-ignore
const template = (e) => html `
	<i @click=${(event) => e.openList(event)}>
		${unsafeHTML(octicons['chevron-down'].toSVG())}
	</i>
  <input-area>
    <select-display @click="${(event) => e.openList(event)}"> ${!!e.value ? e.options.get(e.value) : e.placeholder || ''} </select-display>
    ${e.open
    ? html `<select-options>
          ${Array.from(e.options).map(([key, value]) => html `<select-option @click="${(event) => e.select(key, event)}">${value}</select-option>`)}
        </select-options>`
    : ''}
  </input-area>
`;
let SelectInput = class SelectInput extends NeonElement {
    constructor() {
        super(...arguments);
        this.open = false;
        this.documentClick = (() => (this.open = false)).bind(this);
    }
    connectedCallback() {
        document.addEventListener('click', this.documentClick);
    }
    disconnectedCallback() {
        document.removeEventListener('click', this.documentClick);
    }
    draw() {
        render(template(this), this);
    }
    select(key, event) {
        event.stopPropagation();
        this.open = false;
        this.value = key;
        this.trigger('change', this.value);
    }
    openList(event) {
        event.stopPropagation();
        this.open = true;
    }
    close() {
        this.open = false;
    }
};
__decorate([
    Attribute(),
    __metadata("design:type", String)
], SelectInput.prototype, "placeholder", void 0);
__decorate([
    Redraw(),
    __metadata("design:type", Map)
], SelectInput.prototype, "options", void 0);
__decorate([
    Redraw(),
    __metadata("design:type", Boolean)
], SelectInput.prototype, "open", void 0);
__decorate([
    Redraw(),
    __metadata("design:type", Object)
], SelectInput.prototype, "value", void 0);
SelectInput = __decorate([
    Element('select-input')
], SelectInput);
export { SelectInput };
//# sourceMappingURL=select-input.js.map