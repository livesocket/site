import { NeonElement } from '../../lib';
export declare class Toolbar extends NeonElement {
    connectedCallback(): void;
    draw(): void;
    show(): void;
    hide(): void;
}
