var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const octicons = require('octicons');
import { render, html } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { Element, NeonElement, Redraw } from '../../lib';
// prettier-ignore
const template = (e) => html `
	<check-box-outline>
		${!!e.value ? unsafeHTML(octicons['check'].toSVG()) : html `<svg version="1.1" width="12" height="16" viewBox="0 0 12 16"></svg>`}
	</check-box-outline>
`;
let CheckBox = class CheckBox extends NeonElement {
    constructor() {
        super(...arguments);
        this.value = false;
        this._onClick = this.onClick.bind(this);
    }
    connectedCallback() {
        this.addEventListener('click', this._onClick);
        this.draw();
    }
    onClick() {
        this.trigger('change', (this.value = !this.value));
    }
    draw() {
        render(template(this), this);
    }
};
__decorate([
    Redraw(),
    __metadata("design:type", Boolean)
], CheckBox.prototype, "value", void 0);
CheckBox = __decorate([
    Element('check-box')
], CheckBox);
export { CheckBox };
//# sourceMappingURL=check-box.js.map