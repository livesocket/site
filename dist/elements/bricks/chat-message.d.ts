import { NeonElement } from '../../lib';
import { ModChatMessage } from '../../models/ModChatMessage';
export declare class ChatMessage extends NeonElement {
    message: ModChatMessage;
    connectedCallback(): void;
    draw(): void;
}
