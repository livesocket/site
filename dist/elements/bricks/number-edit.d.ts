import { NeonElement } from '../../lib';
export declare class NumberEdit extends NeonElement {
    value: number;
    label: string;
    min: string;
    max: string;
    step: string;
    hint: string;
    editing: boolean;
    connectedCallback(): void;
    draw(): void;
    commit(): void;
}
