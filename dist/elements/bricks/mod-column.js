var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { render, html } from 'lit-html';
import { Element, NeonElement } from '../../lib';
const template = (e) => html `
  <tab-area>
    <tab class=${e.activeTab === void 0 ? 'active' : ''} @click=${() => e.closePanels()}>Home</tab>
    <tab class=${e.activeTab === 'commands' ? 'active' : ''} @click=${() => e.toggleTab('commands')}>Commands</tab>
    <tab class=${e.activeTab === 'config' ? 'active' : ''} @click=${() => e.toggleTab('config')}>Config</tab>
  </tab-area>
  <mod-chat></mod-chat>
  ${!!e.activeTab
    ? html `
        <mod-panel></mod-panel>
      `
    : ''}
`;
let ModColumn = class ModColumn extends NeonElement {
    connectedCallback() {
        this.draw();
    }
    draw() {
        render(template(this), this);
    }
    closePanels() {
        this.activeTab = void 0;
        this.draw();
    }
    toggleTab(name) {
        if (this.activeTab === name) {
            this.activeTab = void 0;
        }
        else {
            this.activeTab = name;
        }
        this.draw();
    }
};
ModColumn = __decorate([
    Element('mod-column')
], ModColumn);
export { ModColumn };
//# sourceMappingURL=mod-column.js.map