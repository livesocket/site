import { NeonElement } from '../../lib';
export declare class ModColumn extends NeonElement {
    activeTab: string;
    connectedCallback(): void;
    draw(): void;
    closePanels(): void;
    toggleTab(name: string): void;
}
