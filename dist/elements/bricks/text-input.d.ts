import { NeonElement } from '../../lib';
export declare class TextInput extends NeonElement {
    value: string;
    placeholder: string;
    connectedCallback(): void;
    draw(): void;
    change(event: Event): void;
}
