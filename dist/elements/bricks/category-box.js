var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { render, html } from 'lit-html';
import { Element, NeonElement, Attribute } from '../../lib';
import { contents } from '../../helpers/contents';
const template = (e) => html `
  <title-area>${e.title}</title-area>
  <content-area>${e.contents}</content-area>
`;
let CategoryBox = class CategoryBox extends NeonElement {
    connectedCallback() {
        this.contents = contents(this);
        this.draw();
    }
    draw() {
        render(template(this), this);
    }
};
__decorate([
    Attribute(),
    __metadata("design:type", String)
], CategoryBox.prototype, "title", void 0);
CategoryBox = __decorate([
    Element('category-box')
], CategoryBox);
export { CategoryBox };
//# sourceMappingURL=category-box.js.map