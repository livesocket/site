import { NeonElement } from '../../lib';
export declare class SelectInput extends NeonElement {
    placeholder: string;
    options: Map<string | number, string>;
    open: boolean;
    value: string | number;
    private documentClick;
    connectedCallback(): void;
    disconnectedCallback(): void;
    draw(): void;
    select(key: string | number, event: Event): void;
    openList(event: Event): void;
    close(): void;
}
