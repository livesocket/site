var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
const octicons = require('octicons');
import { render, html } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { Element, NeonElement, onEvent } from '../../lib';
// prettier-ignore
const template = (e) => html `
  <info-area>
    <item-name>!${e.item.name}</item-name>
    <item-response>
      <text-edit .value=${e.item.response} @commit=${onEvent(response => e.save(e.item.response = response))}></text-edit>
    </item-response>
    <item-description>
      <text-edit .value=${e.item.description} @commit=${onEvent(description => e.save(e.item.description = description))} hint="No description for this command"></text-edit>
    </item-description>
    <item-cooldown>
      Cooldown:
      <number-edit .value=${e.item.cooldown} @commit=${onEvent(cooldown => e.save(e.item.cooldown = cooldown))} min=0 hint="None"></number-edit>
    </item-cooldown>
    <item-cooldown>
      Schedule:
      <number-edit .value=${e.item.schedule} @commit=${onEvent(schedule => e.save(e.item.schedule = schedule))} min=0 hint="None"></number-edit>
    </item-cooldown>
    <item-creator>Created By: ${e.item.createdBy}</item-creator>
  </info-area>
  <action-area>
    <action-item>
      <check-box .value=${e.item.enabled} @change=${onEvent(enabled => e.setEnabled(enabled))}></check-box>
      <action-name>Enabled</action-name>
    </action-item>
    <action-item>
      <check-box .value=${e.item.restricted} @change=${onEvent(restricted => e.setRestricted(restricted))}></check-box>
      <action-name>Restricted</action-name>
    </action-item>
    <action-item>
      <check-box .value=${e.item.scheduled} @change=${onEvent(scheduled => e.setScheduled(scheduled))}></check-box>
      <action-name>Scheduled</action-name>
    </action-item>
    <action-item>
      <button type="button" class="danger" @click=${() => e.delete()}>${unsafeHTML(octicons['trashcan'].toSVG())}</button>
    </action-item>
  </td>
`;
let CommandItem = class CommandItem extends NeonElement {
    constructor() {
        super(...arguments);
        this.service = Neon.get('CommandService');
    }
    connectedCallback() {
        this.draw();
    }
    draw() {
        this.item.enabled ? this.classList.remove('disabled') : this.classList.add('disabled');
        render(template(this), this);
    }
    async setEnabled(enabled) {
        if (this.item.enabled !== enabled) {
            try {
                this.item = await this.service.toggleEnabled(this.item);
                this.draw();
            }
            catch (e) {
                console.error(e);
            }
        }
    }
    async setRestricted(restricted) {
        if (this.item.restricted !== restricted) {
            try {
                this.item = await this.service.toggleRestricted(this.item);
                this.draw();
            }
            catch (e) {
                console.error(e);
            }
        }
    }
    async setScheduled(scheduled) {
        if (this.item.scheduled !== scheduled) {
            try {
                this.item = await this.service.toggleScheduled(this.item);
                this.draw();
            }
            catch (e) {
                console.error(e);
            }
        }
    }
    async save(_) {
        try {
            this.item = await this.service.update(this.item);
            this.draw();
        }
        catch (e) {
            console.error(e);
        }
    }
    async delete() {
        try {
            await this.service.delete(this.item.name);
            this.remove();
        }
        catch (e) {
            console.error(e);
        }
    }
};
CommandItem = __decorate([
    Element('command-item')
], CommandItem);
export { CommandItem };
//# sourceMappingURL=command-item.js.map