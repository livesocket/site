var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { render, html } from 'lit-html';
import { Element, NeonElement, Redraw } from '../../lib';
const template = (e) => html `
  ${e.creating
    ? html `
        <command-new @done=${() => e.refresh((e.creating = false))}></command-new>
      `
    : html `
        <button type="button" @click=${() => (e.creating = true)}>New Command</button>
      `}
  <hr />
  <command-list></command-list>
`;
let CommandHome = class CommandHome extends NeonElement {
    constructor() {
        super(...arguments);
        this.creating = false;
    }
    connectedCallback() {
        this.draw();
    }
    draw() {
        render(template(this), this);
    }
    refresh(...any) {
        this.querySelector('command-list').refresh();
    }
};
__decorate([
    Redraw(),
    __metadata("design:type", Object)
], CommandHome.prototype, "creating", void 0);
CommandHome = __decorate([
    Element('command-home')
], CommandHome);
export { CommandHome };
//# sourceMappingURL=command-home.js.map