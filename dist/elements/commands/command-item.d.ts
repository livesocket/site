import { NeonElement } from '../../lib';
import { Command } from '../../models/Command';
import { CommandService } from '../../services/CommandService';
export declare class CommandItem extends NeonElement {
    item: Command;
    service: CommandService;
    connectedCallback(): void;
    draw(): void;
    setEnabled(enabled: boolean): Promise<void>;
    setRestricted(restricted: boolean): Promise<void>;
    setScheduled(scheduled: boolean): Promise<void>;
    save(_?: any): Promise<void>;
    delete(): Promise<void>;
}
