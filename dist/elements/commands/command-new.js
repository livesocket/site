var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { render, html } from 'lit-html';
import { Element, NeonElement, onEvent } from '../../lib';
const template = (e) => html `
  <form
    @submit=${(event) => {
    event.preventDefault();
    e.submit();
}}
  >
    <text-input placeholder="Name" @change=${onEvent(name => (e.command.name = name))}> </text-input>
    <text-input placeholder="Response" @change=${onEvent(response => (e.command.response = response))}></text-input>
    <text-input
      placeholder="Description"
      @change=${onEvent(description => (e.command.description = description))}
    ></text-input>
    <number-input placeholder="Cooldown" @change=${onEvent(cooldown => (e.command.cooldown = cooldown))}></number-input>
    <button-group>
      <button type="submit">Create Command</button>
      <button
        type="button"
        @click=${() => {
    e.command = {};
    e.trigger('done');
}}
      >
        Cancel
      </button>
    </button-group>
  </form>
`;
let CommandNew = class CommandNew extends NeonElement {
    constructor() {
        super(...arguments);
        this.command = {};
        this.service = Neon.get('CommandService');
    }
    connectedCallback() {
        this.draw();
    }
    draw() {
        render(template(this), this);
    }
    async submit() {
        try {
            this.validate();
            await this.service.create(this.command);
            this.trigger('done');
        }
        catch (e) {
            Neon.handleError(e);
        }
    }
    validate() {
        if (!this.command.name) {
            throw new Error('Command name is required.');
        }
        if (!/^[a-zA-Z0-9]*$/.test(this.command.name)) {
            throw new Error('Command name must not contain any special characters.');
        }
        if (!this.command.response) {
            throw new Error('Command response is required.');
        }
    }
};
CommandNew = __decorate([
    Element('command-new')
], CommandNew);
export { CommandNew };
//# sourceMappingURL=command-new.js.map