import { NeonElement } from '../../lib';
import { Command } from '../../models/Command';
export declare class CommandNew extends NeonElement {
    command: Command;
    private service;
    connectedCallback(): void;
    draw(): void;
    submit(): Promise<void>;
    private validate;
}
