var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { render, html } from 'lit-html';
import { Element, NeonElement } from '../../lib';
// prettier-ignore
const template = (e) => html `
  <hint-text>Click on a field to edit</hint-text>
	${e.commands.map(command => html `<command-item .item=${command}></command-item>`)}
`;
let CommandList = class CommandList extends NeonElement {
    constructor() {
        super(...arguments);
        this.service = Neon.get('CommandService');
    }
    async connectedCallback() {
        this.refresh();
    }
    draw() {
        render(template(this), this);
    }
    async refresh() {
        this.commands = await this.service.get();
        this.draw();
    }
};
CommandList = __decorate([
    Element('command-list')
], CommandList);
export { CommandList };
//# sourceMappingURL=command-list.js.map