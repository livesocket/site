import { NeonElement } from '../../lib';
export declare class CommandHome extends NeonElement {
    creating: boolean;
    connectedCallback(): void;
    draw(): void;
    refresh(...any: any): void;
}
