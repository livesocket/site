import { NeonElement } from '../../lib';
import { Command } from '../../models/Command';
export declare class CommandList extends NeonElement {
    commands: Command[];
    private service;
    connectedCallback(): Promise<void>;
    draw(): void;
    refresh(): Promise<void>;
}
