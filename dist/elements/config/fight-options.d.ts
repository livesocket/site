import { NeonElement } from '../../lib';
import { FightConfig, FightService } from '../../services/FightService';
export declare class FightOptions extends NeonElement {
    config: FightConfig;
    service: FightService;
    connectedCallback(): Promise<void>;
    draw(): void;
    save(_?: any): Promise<void>;
}
