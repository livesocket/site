import { NeonElement } from '../../lib';
import { RollService, RollConfig } from '../../services/RollService';
export declare class RollOptions extends NeonElement {
    config: RollConfig;
    service: RollService;
    connectedCallback(): Promise<void>;
    draw(): void;
    save(_?: any): Promise<void>;
}
