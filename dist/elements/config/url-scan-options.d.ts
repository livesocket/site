import { NeonElement } from '../../lib';
import { UrlScan } from '../../models/UrlScan';
import { UrlScanService } from '../../services/UrlScanService';
export declare class UrlScanOptions extends NeonElement {
    config: UrlScan;
    service: UrlScanService;
    connectedCallback(): Promise<void>;
    draw(): void;
    save(_?: any): Promise<void>;
}
