var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { render, html } from 'lit-html';
import { Element, NeonElement, onEvent } from '../../lib';
const template = (e) => html `
  <category-box title="Roll Options">
    <section>
      <label>Roll Enabled</label>
      <check-box .value=${e.config.enabled} @change=${onEvent(value => e.save((e.config.enabled = value)))}></check-box>
    </section>
    <section>
      <label>Roll Cooldown:</label>
      <number-edit
        .value=${e.config.cooldown}
        @commit=${onEvent(cooldown => e.save((e.config.cooldown = cooldown || 0)))}
        hint="None"
        min="0"
      ></number-edit>
    </section>
  </category-box>
`;
let RollOptions = class RollOptions extends NeonElement {
    constructor() {
        super(...arguments);
        this.service = Neon.get('RollService');
    }
    async connectedCallback() {
        try {
            this.config = await this.service.get();
            this.draw();
        }
        catch (e) {
            Neon.handleError(e);
        }
    }
    draw() {
        render(template(this), this);
    }
    async save(_) {
        await this.service.update(this.config);
        this.draw();
    }
};
RollOptions = __decorate([
    Element('roll-options')
], RollOptions);
export { RollOptions };
//# sourceMappingURL=roll-options.js.map