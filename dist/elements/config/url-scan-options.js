var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { render, html } from 'lit-html';
import { Element, NeonElement, onEvent } from '../../lib';
import { PermissionLevelMap } from '../../models/PermissionLevel';
const template = (e) => html `
  <category-box title="Url Scan Options">
    <section>
      <label>Scanning Enabled</label>
      <check-box .value=${e.config.enabled} @change=${onEvent(value => e.save((e.config.enabled = value)))}></check-box>
    </section>
    <section>
      <label>Minimum Level</label>
      <select-input
        .value=${e.config.permit}
        .options=${PermissionLevelMap}
        @change=${onEvent(value => e.save((e.config.permit = value)))}
        placeholder="Choose minimum level"
      ></select-input>
    </section>
    <section>
      <label>Use Whitelist</label>
      <check-box
        .value=${e.config.useWhitelist}
        @change=${onEvent(value => e.save((e.config.useWhitelist = value)))}
      ></check-box>
    </section>
    <section>
      <label>Whitelist</label>
      <text-input
        .value=${e.config.whitelist}
        @change=${onEvent(value => e.save((e.config.whitelist = value)))}
      ></text-input>
    </section>
    <section>
      <label>Timeout:</label>
      <number-edit
        .value=${e.config.timeout}
        @commit=${onEvent(timeout => e.save((e.config.timeout = timeout || 0)))}
        hint="None"
        min="0"
      ></number-edit>
    </section>
  </category-box>
`;
let UrlScanOptions = class UrlScanOptions extends NeonElement {
    constructor() {
        super(...arguments);
        this.service = Neon.get('UrlScanService');
    }
    async connectedCallback() {
        try {
            this.config = await this.service.get();
            this.draw();
        }
        catch (e) {
            Neon.handleError(e);
        }
    }
    draw() {
        render(template(this), this);
    }
    async save(_) {
        await this.service.update(this.config);
        this.draw();
    }
};
UrlScanOptions = __decorate([
    Element('url-scan-options')
], UrlScanOptions);
export { UrlScanOptions };
//# sourceMappingURL=url-scan-options.js.map