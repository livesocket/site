import { NeonElement } from '../lib';
export declare class Home extends NeonElement {
    connectedCallback(): void;
    draw(): void;
}
