import { NeonElement } from '../../lib';
export declare class Login extends NeonElement {
    private service;
    connectedCallback(): void;
    draw(): void;
    login(): Promise<void>;
}
