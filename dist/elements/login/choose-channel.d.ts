import { NeonElement } from '../../lib';
export declare class ChooseChannel extends NeonElement {
    connectedCallback(): void;
    draw(): void;
    setChannel(name: string): void;
}
