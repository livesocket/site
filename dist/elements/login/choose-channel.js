var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { render, html } from 'lit-html';
import { Element, NeonElement } from '../../lib';
import { channels, channel } from '../../helpers/channel';
import { admin } from '../../helpers/admin';
// prettier-ignore
const template = (e) => html `
  <h1>Choose channel to moderate</h1>
  <channel-grid>
    ${admin() ? html `<admin-button @click=${() => Neon.navigate('admin-home')}><channel-button-overlay><name-area>admin area</name-area></channel-button-overlay></admin-button>` : ''}
    ${channels().map(channel => html `<channel-button channel="${channel}" @click=${e.setChannel.bind(e, channel)}></channel-button>`)}
  </button-group>
`;
let ChooseChannel = class ChooseChannel extends NeonElement {
    connectedCallback() {
        this.draw();
    }
    draw() {
        render(template(this), this);
    }
    setChannel(name) {
        channel(name);
        Neon.navigate('home-');
    }
};
ChooseChannel = __decorate([
    Element('choose-channel')
], ChooseChannel);
export { ChooseChannel };
//# sourceMappingURL=choose-channel.js.map