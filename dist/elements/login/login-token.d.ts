import { NeonElement } from '../../lib';
export declare class Login extends NeonElement {
    access_token: string;
    state: string;
    private service;
    connectedCallback(): Promise<void>;
}
