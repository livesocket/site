var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { render, html } from 'lit-html';
import { Element, NeonElement } from '../../lib';
const template = (e) => html `
  <h1>Login with your Twitch account</h1>
  <p>Clicking login will direct you to twitch</p>
  <button type="button" @click=${e.login.bind(e)}>Login</button>
`;
let Login = class Login extends NeonElement {
    constructor() {
        super(...arguments);
        this.service = Neon.get('LoginService');
    }
    connectedCallback() {
        this.draw();
    }
    draw() {
        render(template(this), this);
    }
    async login() {
        if (!!this.service.token) {
            try {
                if (!!(await this.service.validate())) {
                    await this.service.login();
                }
            }
            catch (e) {
                Neon.handleError(e);
            }
        }
        else {
            await this.service.authorize();
        }
    }
};
Login = __decorate([
    Element('login-')
], Login);
export { Login };
//# sourceMappingURL=login.js.map