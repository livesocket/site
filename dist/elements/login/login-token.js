var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Element, NeonElement, Attribute } from '../../lib';
let Login = class Login extends NeonElement {
    constructor() {
        super(...arguments);
        this.service = Neon.get('LoginService');
    }
    async connectedCallback() {
        try {
            await this.service.login(this.access_token, this.state);
        }
        catch (e) {
            Neon.handleError(e);
        }
    }
};
__decorate([
    Attribute(),
    __metadata("design:type", String)
], Login.prototype, "access_token", void 0);
__decorate([
    Attribute(),
    __metadata("design:type", String)
], Login.prototype, "state", void 0);
Login = __decorate([
    Element('login-token')
], Login);
export { Login };
//# sourceMappingURL=login-token.js.map