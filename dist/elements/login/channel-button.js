var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { render, html } from 'lit-html';
import { Element, NeonElement, Attribute } from '../../lib';
const template = (e) => html `
  <channel-button-overlay>
    <name-area>${e.channel}</name-area>
  </channel-button-overlay>
`;
let ChannelButton = class ChannelButton extends NeonElement {
    connectedCallback() {
        this.style.backgroundImage = "url('/assets/code-background.png')";
        this.draw();
    }
    draw() {
        render(template(this), this);
    }
};
__decorate([
    Attribute(),
    __metadata("design:type", String)
], ChannelButton.prototype, "channel", void 0);
ChannelButton = __decorate([
    Element('channel-button')
], ChannelButton);
export { ChannelButton };
//# sourceMappingURL=channel-button.js.map