import { NeonElement } from '../../lib';
export declare class ChannelButton extends NeonElement {
    channel: string;
    connectedCallback(): void;
    draw(): void;
}
