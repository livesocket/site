var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { html, render } from 'lit-html';
import { Element, NeonElement } from '../../lib';
const template = () => html `
  <h1>Admin Panel</h1>
  <h2>Careful these options are dangerous</h2>
`;
let AdminWelcome = class AdminWelcome extends NeonElement {
    connectedCallback() {
        render(template(), this);
    }
};
AdminWelcome = __decorate([
    Element('admin-welcome')
], AdminWelcome);
export { AdminWelcome };
//# sourceMappingURL=welcome.js.map