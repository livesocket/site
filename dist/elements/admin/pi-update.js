var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { render, html } from 'lit-html';
import { Element, PopupElement, onEvent } from '../../lib';
const template = (e) => html `
  <form @submit=${(event) => e.submit(event)}>
    <form-display label="Channel" value=${e.pi.channel}></form-display>
    <form-display label="ID" value=${e.pi.id}></form-display>
    <form-display label="Project" value=${e.pi.project}></form-display>
    <text-input placeholder="New Version" @change=${onEvent(x => (e.newVersion = x))}></text-input>
    <button type="submit">Send Update</button>
  </form>
`;
let AdminPiUpdate = class AdminPiUpdate extends PopupElement {
    connectedCallback() {
        render(template(this), this);
    }
    async submit(event) {
        event.preventDefault();
        try {
            await Neon.get('PiService').update(this.pi.id, this.newVersion);
            this.complete();
        }
        catch (error) {
            Neon.handleError(error);
        }
    }
};
AdminPiUpdate = __decorate([
    Element('admin-pi-update')
], AdminPiUpdate);
export { AdminPiUpdate };
//# sourceMappingURL=pi-update.js.map