import { PopupElement } from '../../lib';
import { Pi } from '../../models/Pi';
export declare class AdminPiUpdate extends PopupElement {
    pi: Pi;
    newVersion: string;
    connectedCallback(): void;
    submit(event: Event): Promise<void>;
}
