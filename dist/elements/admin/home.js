var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { html, render } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { Element, NeonElement } from '../../lib';
const template = (e) => html `
  <admin-sidebar>
    <ul>
      <li @click=${() => e.open('admin-channels')}>Channels</li>
      <li @click=${() => e.open('admin-commands')}>Commands</li>
      <li @click=${() => e.open('admin-pi')}>Pis</li>
    </ul>
  </admin-sidebar>
  <admin-container>
    ${unsafeHTML(e.openPage)}
  </admin-container>
`;
let AdminHome = class AdminHome extends NeonElement {
    constructor() {
        super(...arguments);
        this.openPage = '<admin-welcome></admin-welcome>';
    }
    connectedCallback() {
        this.draw();
    }
    draw() {
        render(template(this), this);
    }
    open(el) {
        this.openPage = `<${el}></${el}>`;
        this.draw();
    }
};
AdminHome = __decorate([
    Element('admin-home')
], AdminHome);
export { AdminHome };
//# sourceMappingURL=home.js.map