import { NeonElement } from '../../lib';
export declare class AdminHome extends NeonElement {
    openPage: string;
    connectedCallback(): void;
    draw(): void;
    open(el: string): void;
}
