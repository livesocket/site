import { NeonElement } from '../../lib';
import { Pi } from '../../models/Pi';
export declare class AdminPi extends NeonElement {
    pis: Pi[];
    connectedCallback(): Promise<void>;
    draw(): void;
    update(pi: Pi): void;
}
