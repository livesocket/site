var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { html, render } from 'lit-html';
import { Element, NeonElement } from '../../lib';
const template = (e) => html `
  <table>
    <thead>
      <tr>
        <th>Channel</th>
        <th>ID</th>
        <th>Project</th>
        <th>Last Ping</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      ${e.pis.map(x => html `
          <tr>
            <td>${x.channel}</td>
            <td>${x.id}</td>
            <td>${x.project}</td>
            <td>${x.timestamp.toFormat('f')}</td>
            <td>
              <button type="button" @click=${() => e.update(x)}>Update</button>
            </td>
          </tr>
        `)}
    </tbody>
  </table>
`;
let AdminPi = class AdminPi extends NeonElement {
    async connectedCallback() {
        this.pis = await Neon.get('PiService').get();
        this.draw();
    }
    draw() {
        render(template(this), this);
    }
    update(pi) {
        Neon.popup.show('admin-pi-update', { pi });
    }
};
AdminPi = __decorate([
    Element('admin-pi')
], AdminPi);
export { AdminPi };
//# sourceMappingURL=pi.js.map